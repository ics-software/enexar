# ENeXAr - The EPICS NeXus Archiver

ENeXAr is a Python service that can be used to archive PVs into NeXus files.

It implements an EPICS PV Access RPC server that can receive the following commands (see also http://ics-software.pages.esss.lu.se/enexar):
- OPEN: before saving data to a file, the file must be opened. This is to avoid adding data to an already existing file by mistake. If the file does not exist, the file is opened automatically when an acquisition starts and this command is not needed. It only takes one argument:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5"

- ACQUIRE: Start an acquisition of a given PV to a certain NeXus file. It can take either a predefined number of measurements, or run continuously until the acquisition is stopped using the STOP or the CLOSE commands. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5".
    - pv: the name of the PV signal. E.g., "PV:SIGNAL".
    - n_acq (optional): to define the number of measurements that will be taken. Default is 1.
    - continuous (optional): set this flag to `True` for continuous acquisition. Default is `False`.
    - use_groups (optional): set this flag to `True` to create a structure with one group per PV under the `data` group. Otherwise, all acquisitions are stored under the `data` group without using subgroups. Default is `True`.
    - timeout (optional): to define a timeout for each acquisition (in seconds). Default is 4 seconds, to trigger before the default timeout of `pvcall`.
    - metadata (optional): all the NeXus metadata in JSON format. For a detailed description, see below.
    - ts (optional): EPICS timestamp filter*. If set to `True`, it will enable the timestamp filter that sets the timestamp to the time when the request was done, instead of when the record was processed.
    - dbnd (optional): EPICS deadband filter*. To receive monitor updates only when the PV changes by an amount larger than the deadband. It is defined as a JSON string with the same format as in the EPICS documentation.
    - arr (optional): EPICS array filter*. To get a subset of an array, defined by the starting index `s`, the end index `e`, and the increment `i`. It is defined as a JSON string with the same format as in the EPICS documentation. The shorthand notation is not accepted.
    - sync (optional): EPICS synchronize filter*. To synchronize the monitor updates with respect to an internal IOC state. It is defined as a JSON string with the same format as in the EPICS documentation.
    - dec (optional): EPICS decimate filter*. The monitor will update only after the record is processed `dec` times. It takes an integer number.

- ACQUIRE_S: synchronous version of the ACQUIRE command. The RPC call will block and will return only when the acquisition is done. It takes the same arguments.

- IS_OPEN: Returns True if a file is open. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5"

- IS_ACQUIRING: Returns True if the PV is being acquired to a given path. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5"
    - pv: the name of the PV signal. E.g., "PV:SIGNAL".

- STOP: Stops an acquisition. It works with both LOG and LOGNWAIT commands. The file will continue to accept more data. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5"
    - pv: the name of the PV signal. E.g., "PV:SIGNAL".

- CLOSE: closes the HDF5 file passed as an argument. If there is any acquisition running, they are stopped. After the file is closed, no more data can be added to that file. It only takes one argument:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5"

The service also provides a STATUS PV where one can check the acquisitions that are currently taking place, plus some extra information:
 - Path: location where data is stored.
 - PV: signal name.
 - Mode: it can be CONT, for continuous acquisitions; N_ACQ=#, for acquisitions limited in time; or OPEN, when no acquisition is taking place but the file is still accepting new datasets.
 - User: username that did the request (it may be replaced by a gateway).
 - Host: address where the request originated (it may be replaced by a gateway).
 - Time: timestamp when the request was received.

\* For EPICS filters, check also the official EPICS documentation at https://epics.anl.gov/base/R7-0/4-docs/filters.html .
 
### Deprecated commands
The following commands were available in the first release candidates, but were deprecated in favor of the ACQUIRE and ACQUIRE_S commands. They are kept for compatibility with WeTest, until a new version is released.
- LOG: Saves continuously data until a CLOSE command is received. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5".
    - pv: the name of the PV signal. E.g., "PV:SIGNAL".
    - metadata (optional): all the NeXus metadata in JSON format. For a detailed description, see below.
    - timeout (optional): to define a timeout for each acquisition (in seconds). Default is 4 seconds, to trigger before the default timeout of `pvcall`.

- LOGNWAIT: Takes a number of acquisitions and saves them to a file. The command returns when the data is saved. It takes the following arguments:
    - path: the path to the HDF5 file. E.g., "path/to/file.h5".
    - pv: the name of the PV signal. E.g., "PV:SIGNAL".
    - n_acq (optional, default=1): number of acquisitions to save.
    - metadata (optional): all the NeXus metadata in JSON format. For a detailed description, see below.
    - timeout (optional): to define a timeout for each acquisition (in seconds). Default is 4 seconds, to trigger before the default timeout of `pvcall`.

## Cloning the repository
This repository uses pre-commit to run black and flake8. Remember to run `pre-commit install` manually after cloning the repository.

## ESS instances
To see details about the instances currently running at ESS, please check the following Confluence Page:

https://confluence.esss.lu.se/display/SW/ENeXAr+-+The+EPICS+NeXus+Archiver

## EPICS7 support
ENeXAr can connect to PVs using both PV Access and Channel Access. By default it will try to connect using PV Access. To use CA, append the prefix "ca://" to the PV signal. The prefix "pva://" is also supported for PV Access, although not required.

## Data Structure
The PV data is saved in the /entry/data group. Each acquisition is stored in a different dataset, that takes the name of the PV signal, with "_index" appended if more than 1 acquisition is taken. Instead of the PV signal, a different name can be defined in the metadata argument (see below).

## Metadata
The metadata is expected to follow the following convention:
https://confluence.esss.lu.se/display/BIG/Data+format+proposal

All the metadata is optional, and default values will be used if not provided. The server will include into the NeXus metadata any information contained in the following 3 structures (represented by a set of JSON key-value pairs in command-line):
- "file_entry": the attributes of the NXentry. It takes the following attributes: "run_cycle", "entry_identifier", "title", "description", and "start_time".
- "file_user": the attributes of the NXuser entry. It takes: "name", "role", "affiliation", and "facility_user_id".
- "instruments": it takes a set of instrument, represented as structures with name the name of the instrument, and value a substructure containing the following key-value pairs: "nxtype", "name",  "location", "description", "channel", "epics_channels", "value", and any other user-defined attributes. See examples above for clarity.
 - "data": a set of attributes that will be added to each dataset. By default, the timestamp and the PV signal are automatically included. The special key "dataset" will be used to override the default name of the dataset (PV signal name). E.g., metadata={"data": {"dataset": "my_waveform"}}

## Examples

### Client
Requests to log data are done using PV Access. Here we give examples on how to use the command-line tool pvcall to send requests, assuming that the server prefix is "ENEXAR:".

Before starting the tests, one can run a monitor to "ENEXAR:STATUS" to see how commands are executed
```bash
$ pvmonitor ENEXAR:STATUS
```

- Take 1 acquisition from "BCM-01:SIGNAL:001" and save it to "measurements/test.h5":
```bash
$ pvcall ENEXAR:ACQUIRE_S pv=BCM-01:SIGNAL:001 path="measurements/test.h5" \
metadata='{"file_user":{"name":"juan","affiliation":"ESS"},
"file_entry":{"title":"Test","description": "Testing logger"}, 
"instruments":{"BCM1":{"nxtype": "NXbeam_instrumentation", "name":"Beam Current Monitor"}}}' 
```
The HDF5 file is automatically closed to avoid corruption, but the ENeXAr makes it possible to add more data to the same file until it receives a CLOSE command.

The STATUS PV will show this information:
```
ENEXAR:STATUS 2020-07-01 14:10:47.000    
                Path                PV    Mode     Acquired                   User                Host                  Time
measurements/test.h5 BCM-01:SIGNAL:001 N_ACQ=1            0     juanfestebanmuller 192.168.0.147:57320 "2020-07-01 14:10:47"
```
and after the measurement is done it changes to:
```
ENEXAR:STATUS 2020-07-01 14:10:48.000   
                Path PV Mode     Acquired User  Host                  Time
measurements/test.h5    OPEN                         "2020-07-01 14:10:48"
```

- Continuously acquire using Channel Access from "FC-01:SIGNAL:002" in the background and store data in "measurements/test_FC.h5":
```bash
$ pvcall ENEXAR:ACQUIRE continuous=True pv=ca://FC-01:SIGNAL:002 \
path="measurements/test_FC.h5" \
metadata='{"file_user":{"name":"juan","affiliation":"ESS"},
"file_entry":{"title":"Test","description": "Testing logger"},
"instruments":{"FC":{"nxtype": "NXbeam_instrumentation", "name":"Faraday Cup"}}}' 
```

The STATUS PV will show this information:
```
ENEXAR:STATUS 2020-07-01 14:14:36.000    
                   Path                    PV Mode     Acquired                   User                Host                  Time
measurements/test_FC.h5 ca://FC-01:SIGNAL:002 CONT            2     juanfestebanmuller 192.168.0.147:57390 "2020-07-01 14:14:36"  
   measurements/test.h5                       OPEN                                                         "2020-07-01 14:10:48"
```
- Take 3 acquisitions from "BPM-02:SIGNAL:001" and save them also in "measurements/test.h5":
```bash
$ pvcall ENEXAR:ACQUIRE_S pv=BPM-02:SIGNAL:001\
n_acq=3 path="measurements/test.h5"\
metadata='{"file_user":{"name":"juan","affiliation":"ESS"},
"file_entry":{"title":"Test","description": "Testing logger"},
"instruments":{"BPM":{"nxtype": "NXbeam_instrumentation", "name":"Beam Position Monitor", "serial_number": 123456789}},
"data": {"dataset": "BPM-02"}}' 
```
Note that here new datasets and more metadata have been added to the same file. This time, the datasets will be named "faraday_cup_" plus an index, according to the metadata.

The STATUS PV will show this information:
```
ENEXAR:STATUS 2020-07-01 14:16:41.000    
                   Path                     PV     Mode      Acquired                  User                Host                  Time
measurements/test_FC.h5  ca://FC-01:SIGNAL:002     CONT           125    juanfestebanmuller 192.168.0.147:57390 "2020-07-01 14:16:41"
   measurements/test.h5      BPM-02:SIGNAL:001  N_ACQ=3             0    juanfestebanmuller 192.168.0.147:57442 "2020-07-01 14:16:41"
```
and after the new 3 acquisitions are done, it would change to:
```
ENEXAR:STATUS 2020-07-01 14:16:44.000 
                   Path                    PV    Mode      Acquired                  User                Host                  Time
measurements/test_FC.h5 ca://FC-01:SIGNAL:002    CONT           128    juanfestebanmuller 192.168.0.147:57390 "2020-07-01 14:14:44"
   measurements/test.h5                          OPEN                                                         "2020-07-01 14:16:44"
```

- Closing definitely the file "measurements/test.h5", so that no more data can be added:
```bash
$ pvcall ENEXAR:CLOSE path="measurements/test.h5"
```
- Stopping the continuous acquisition from "FC-01:SIGNAL:002":
```bash
$ pvcall ENEXAR:STOP pv=ca://FC-01:SIGNAL:002 path="measurements/test_FC.h5"
```
- and closing definitely the file "measurements/test_FC.h5", so that no more data can be added:
```bash
$ pvcall ENEXAR:CLOSE path="measurements/test_FC.h5"
```
After this the STATUS table is clear again.

## Python client
A Python module has been developed to simplify the usage of ENeXAr from scripts. It can be found here:
https://gitlab.esss.lu.se/ics-software/enexar-cli .

## Running a server
### Deploying ENeXAr using Docker (recommended)
First pull the latest image (or selected version) of the Docker image:
```
$ docker pull registry.esss.lu.se/ics-software/enexar:latest
```
This command is also valid to update the image to the latest version.

Then, set the environment variables `$ENEXAR_DATA_PATH` with the absolute path of the directory where the data should be saved (must be writable by csi user), and `$ENEXAR_PREFIX` with the prefix of the ENeXAr instance.

Finally, run the Docker image with:
```
$ docker run --rm -d --network host -v $ENEXAR_DATA_PATH:/data registry.esss.lu.se/ics-software/enexar:latest enexar -p $ENEXAR_PREFIX -r /data
```
For other options when starting the ENeXAr service, please check the section `Running ENeXAr` below.

### Installing ENeXAr using pip (not recommended)
ENeXAr requires Python 3.8+.

To install ENeXAr, we recommend to create and activate a conda environment:
```bash
$ conda create -n enexar python=3.8
$ conda activate enexar
```

There are two possibilities to install ENeXAr. The first one is to install it from Artifactory:
```bash
$ pip install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple  enexar
```
Alternatively, one can clone the git repository and install it:
```bash
$ git clone https://gitlab.esss.lu.se/ics-software/enexar.git
$ cd enexar
$ pip install -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple .
```

### Running ENeXAr
To run the service, use the following command:

```bash
$ enexar -r "/path/to/store" -p "PV:PREFIX"
```

The arguments are optional and are the following:
- -r, --root: the path where the NeXus files will be stored. Default is the current working directory.
- -p, --prefix: prefix for the PVs. Default is "ENEXAR:".
- -t, --timeout: to automatically close a file after a period of inactivity. After that, the file won't accept more data.


## Developer
Juan F. Esteban Müller (ESS) <JuanF.EstebanMuller@ess.eu>