FROM continuumio/miniconda3:23.5.2-0

RUN groupadd -r -g 10045 linacshares \
  && useradd --no-log-init -r -g linacshares -u 10045 linacuser

COPY environment.yml /app/environment.yml

RUN conda update -n base conda \
  && conda config --system --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda \
  && conda config --system --add channels conda-forge \
  && conda env create -n enexar -f /app/environment.yml \
  && conda clean -ay

COPY --chown=linacuser:linacshares . /app

# Make sure the /app directory is owned by linacuser
RUN chown -R linacuser:linacshares /app
WORKDIR /app

RUN /opt/conda/envs/enexar/bin/pip install .

RUN mkdir -p /data
RUN chown -R linacuser:linacshares /data

USER linacuser

ENV PATH=/opt/conda/envs/enexar/bin:$PATH
