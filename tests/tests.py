#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import threading
import time
import unittest

import h5py
from enexar.epics_server import EpicsServer
from enexar.file_manager import FileManager
from enexar_cli import EnexarCli
from essnexus import essnexus
from p4p.nt import NTScalar
from p4p.server import Server, StaticProvider
from p4p.server.thread import SharedPV

import xmlrunner


class IntegrationTests(unittest.TestCase):
    prefix = "ENEXAR:TEST:"
    root_path = "test"
    test_pv_name = "test_counter"

    @classmethod
    def setUpClass(cls):
        """
        Running the ENeXAr server that will be used for the test executions.
        """

        if not os.path.exists(cls.root_path):
            try:
                os.makedirs(cls.root_path)
            except Exception:
                raise Exception("Failed to create the dirname to save the data.")

        FileManager.timeout = 15

        cls.epics_server = EpicsServer(cls.prefix, cls.root_path)
        cls.thread = threading.Thread(target=cls.epics_server.start)
        cls.thread.start()

        cls.thread2 = threading.Thread(target=cls.pvServer)
        cls.thread2.start()

    @classmethod
    def pvServer(cls):
        # Counter PV
        counter_pv = SharedPV(nt=NTScalar("i"))
        counter = 0
        counter_pv.open(counter)
        counter_pv_provider = StaticProvider("Counter")
        counter_pv_provider.add(cls.test_pv_name, counter_pv)
        counter_pv_provider.add(
            cls.test_pv_name
            + '.{"ts":{},"dbnd":{"d":1.5,},"arr":{"i":1,},"sync":{"while":"blue"},"dec":{"n":1},}',
            counter_pv,
        )

        # Start the server
        server = Server(providers=[counter_pv_provider])
        cls.stop_flag = False
        with server:
            while not cls.stop_flag:
                counter += 1
                counter_pv.post(counter, timestamp=time.time())
                time.sleep(0.2)

    @classmethod
    def tearDownClass(cls):
        cls.epics_server.stop()
        cls.stop_flag = True

    def test_instantiate_client(self):
        """
        Test that the class can be instantiated.
        """
        self.assertIsInstance(EnexarCli(self.prefix), EnexarCli)

    def test_instantiate_client_error(self):
        """
        Test that the class cannot be instantiated if the server is not running (wrong prefix).
        """
        with self.assertRaises(TimeoutError):
            EnexarCli("")

    def test_open_and_close(self):
        enexar = EnexarCli(self.prefix)
        filename = "test.h5"
        open_r = enexar.open(filename)
        close_r = enexar.close(filename)
        self.assertTrue(open_r)
        self.assertTrue(close_r)
        self.assertTrue(os.path.exists(os.path.join(self.root_path, filename)))

    # No .h5 extension
    def test_open_and_close_no_ext(self):
        enexar = EnexarCli(self.prefix)
        open_r = enexar.open("test_open_and_close_no_ext")
        close_r = enexar.close("test_open_and_close_no_ext")
        self.assertTrue(open_r)
        self.assertTrue(close_r)

    # .hdf5 extension
    def test_open_and_close_hdf5_ext(self):
        enexar = EnexarCli(self.prefix)
        open_r = enexar.open("test_open_and_close_hdf5_ext.hdf5")
        close_r = enexar.close("test_open_and_close_hdf5_ext.hdf5")
        self.assertTrue(open_r)
        self.assertTrue(close_r)

    def test_open_and_close_subdir(self):
        enexar = EnexarCli(self.prefix)
        dirname = os.path.join(self.root_path, "subdir")
        if os.path.exists(dirname):
            shutil.rmtree(dirname)
        filename = "subdir/test.h5"
        open_r = enexar.open(filename)
        close_r = enexar.close(filename)
        self.assertTrue(open_r)
        self.assertTrue(close_r)
        self.assertTrue(os.path.exists(os.path.join(self.root_path, filename)))

    def test_is_open(self):
        enexar = EnexarCli(self.prefix)
        enexar.open("test.h5")
        is_open1 = enexar.is_open("test.h5")
        enexar.close("test.h5")
        is_open2 = enexar.is_open("test.h5")
        self.assertTrue(is_open1)
        self.assertFalse(is_open2)

    def test_close_unopened(self):
        enexar = EnexarCli(self.prefix)
        with self.assertRaises(Exception):
            enexar.close("test_error.h5")

    def test_stop_no_acquired(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_stop_no_acquired.h5"
        with self.assertRaises(Exception):
            enexar.stop(filename, self.test_pv_name)

    # 1 acq sync
    def test_acquire_s_1(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_1.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # 1 acq async
    def test_acquire_1(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_1.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name)

        # the is_acquiring method is not useful for a fast single acquisition
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is True:
            time.sleep(0.1)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # 10 acq sync
    def test_acquire_s_10(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_10.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name, n_acq=10)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 10)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # 10 acq sync no groups
    def test_acquire_s_10_nogroups(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_10_nogroups.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name, n_acq=10, use_groups=False)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"]
        self.assertTrue(len(data) == 10)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # 10 acq async
    def test_acquire_10(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_10.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name, n_acq=10)

        # Delay to make sure the acquisition started
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        # Then wait for the acquisition to finish...
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is True:
            time.sleep(0.2)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) == 10)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # 10 acq and stop sync
    def test_acquire_s_10_stop(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_10_stop.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        thread = threading.Thread(
            target=enexar.acquire_s,
            args=(
                filename,
                self.test_pv_name,
                10,
            ),
        )
        thread.start()

        # Delay to make sure the acquisition started
        time.sleep(0.1)
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        enexar.stop(filename, self.test_pv_name)

        time.sleep(0.5)

        self.assertTrue(os.path.exists(path))

        thread.join()

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) < 10)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()
        self.assertFalse(enexar.is_acquiring(filename, self.test_pv_name))

    # 10 acq and stop async
    def test_acquire_10_stop(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_10_stop.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name, n_acq=10)

        #     # Delay to make sure the acquisition started
        time.sleep(0.5)
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.5)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        enexar.stop(filename, self.test_pv_name)

        time.sleep(0.5)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) < 10)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()
        self.assertFalse(enexar.is_acquiring(filename, self.test_pv_name))

    # continuous acq and stop sync
    def test_acquire_s_continuous_stop(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_continuous_stop.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        thread = threading.Thread(
            target=enexar.acquire_s,
            args=(
                filename,
                self.test_pv_name,
                0,
                True,
            ),
        )
        thread.start()

        # Delay to make sure the acquisition started
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        enexar.stop(filename, self.test_pv_name)

        time.sleep(0.5)

        self.assertTrue(os.path.exists(path))

        thread.join()

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) > 0)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        self.assertFalse(enexar.is_acquiring(filename, self.test_pv_name))

    # continuous acq and stop async
    def test_acquire_continuous_stop(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_continuous_stop.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name, continuous=True)

        #     # Delay to make sure the acquisition started
        time.sleep(0.1)
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))
        enexar.stop(filename, self.test_pv_name)

        time.sleep(0.5)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) > 0)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        self.assertFalse(enexar.is_acquiring(filename, self.test_pv_name))

    # continuous acq and close async
    def test_acquire_continuous_close(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_continuous_close.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name, continuous=True)

        #     # Delay to make sure the acquisition started
        time.sleep(0.1)
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        enexar.close(filename)

        time.sleep(0.5)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]

        self.assertTrue(len(data) > 0)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        self.assertFalse(enexar.is_acquiring(filename, self.test_pv_name))

    # Two consecutive acquisitions into same file sync
    # To test that the NeXus file is close and reopened
    def test_acquire_s_two_consecutive_acqs_same_file(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_two_consecutive_acqs_same_file.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        enexar.stop(filename, self.test_pv_name)

        time.sleep(1)

        # Second acquisition
        enexar.open(filename)
        enexar.acquire_s(filename, self.test_pv_name)
        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 2)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # Adding data closed file sync
    def test_acquire_s_adding_closed_file(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_adding_closed_file.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        enexar.close(filename)
        # Second acquisition
        enexar.open(filename)
        enexar.acquire_s(filename, self.test_pv_name)
        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 2)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

    # Adding data closed file sync - error
    def test_acquire_s_adding_closed_file_error(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_adding_closed_file_error.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire_s(filename, self.test_pv_name)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        for key in data.keys():
            self.assertTrue(key.startswith(self.test_pv_name))
        h5file.close()

        enexar.close(filename)
        # Trying second acquisition on closed file
        with self.assertRaises(Exception):
            enexar.acquire_s(filename, self.test_pv_name)

    # Add data to a corrupted file
    def test_add_data_corrupted_file(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_add_data_corrupted_file.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        nexusFile = essnexus.File(path)
        nexusFile.h5file.pop("entry")
        nexusFile.close()

        enexar.open(filename)

        with self.assertRaises(Exception):
            enexar.acquire_s(filename, self.test_pv_name)

    # open, remove file and try to acquire
    def test_acquire_s_removed_file(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_removed_file.h5"
        enexar.open(filename)

        path = os.path.join(self.root_path, filename)
        os.remove(path)

        with self.assertRaises(Exception):
            enexar.acquire_s(filename, self.test_pv_name)

    # Trying to acquire twice the same PV at the same time in the same file
    def test_acquire_s_same_PV_twice(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_same_PV_twice.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)

        thread = threading.Thread(
            target=enexar.acquire_s,
            args=(
                filename,
                self.test_pv_name,
                0,
                True,
            ),
        )
        thread.start()

        # Delay to make sure the acquisition started
        while bool(enexar.is_acquiring(filename, self.test_pv_name)) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        # Second acquisition
        with self.assertRaises(Exception):
            enexar.acquire(filename, self.test_pv_name)

        enexar.close(filename)

        thread.join()

    # Trying to acquire twice the same PV at the same time in different files
    def test_acquire_same_PV_twice_diff_files(self):
        enexar = EnexarCli(self.prefix)
        filename_1 = "test_acquire_same_PV_twice_diff_files_1.h5"
        filename_2 = "test_acquire_same_PV_twice_diff_files_2.h5"
        for filename in [filename_1, filename_2]:
            path = os.path.join(self.root_path, filename)
            if os.path.exists(path):
                os.remove(path)
        enexar.acquire(filename_1, self.test_pv_name, continuous=True)

        time.sleep(1)

        self.assertTrue(enexar.is_acquiring(filename_1, self.test_pv_name))

        # Second acquisition
        enexar.acquire(filename_2, self.test_pv_name, continuous=True)

        time.sleep(1)

        self.assertTrue(enexar.is_acquiring(filename_2, self.test_pv_name))

        enexar.close(filename_1)
        enexar.close(filename_2)

    # Trying to acquire twice the same PV at the same time in different files
    def test_acquire_s_same_PV_twice_diff_files(self):
        enexar = EnexarCli(self.prefix)
        filename_1 = "test_acquire_s_same_PV_twice_diff_files_1.h5"
        filename_2 = "test_acquire_s_same_PV_twice_diff_files_2.h5"
        for filename in [filename_1, filename_2]:
            path = os.path.join(self.root_path, filename)
            if os.path.exists(path):
                os.remove(path)

        thread_1 = threading.Thread(
            target=enexar.acquire_s,
            args=(
                filename_1,
                self.test_pv_name,
                0,
                True,
            ),
        )
        thread_1.start()

        time.sleep(1)

        self.assertTrue(enexar.is_acquiring(filename_1, self.test_pv_name))

        # Second acquisition
        thread_2 = threading.Thread(
            target=enexar.acquire_s,
            args=(
                filename_2,
                self.test_pv_name,
                0,
                True,
            ),
        )
        thread_2.start()

        time.sleep(1)

        self.assertTrue(enexar.is_acquiring(filename_2, self.test_pv_name))

        enexar.close(filename_1)
        enexar.close(filename_2)

        thread_1.join()
        thread_2.join()

    # Trying to acquire twice the same PV at the same time in the same file
    def test_acquire_same_PV_twice(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_same_PV_twice.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, self.test_pv_name, continuous=True)

        time.sleep(1)

        self.assertTrue(enexar.is_acquiring(filename, self.test_pv_name))

        enexar.is_acquiring(filename, self.test_pv_name)

        # Second acquisition
        with self.assertRaises(Exception):
            enexar.acquire(filename, self.test_pv_name)

        enexar.close(filename)

    # Adding metadata sync
    def test_acquire_s_metadata(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_metadata.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)

        metadata = {
            "file_user": {"name": "juan", "affiliation": "ESS"},
            "file_entry": {"title": "Test", "description": "Testing logger"},
            "instruments": {
                "INSTRUMENT_1": {
                    "nxtype": "NXbeam_instrumentation",
                    "name": "Instrument name",
                }
            },
        }

        enexar.acquire_s(filename, self.test_pv_name, metadata=metadata)

        self.assertTrue(os.path.exists(path))

        h5file = h5py.File(path, "r")
        data = h5file["entry"]["data"][self.test_pv_name]
        self.assertTrue(len(data) == 1)
        self.assertTrue(h5file["entry"]["user"].attrs["name"] == "juan")
        self.assertTrue(h5file["entry"]["user"].attrs["affiliation"] == "ESS")
        self.assertTrue(h5file["entry"].attrs["title"] == "Test")
        self.assertTrue(
            h5file["entry"].attrs["experiment_description"] == "Testing logger"
        )
        self.assertTrue(
            h5file["entry"]["instruments"]["INSTRUMENT_1"].attrs["NXClass"]
            == "NXbeam_instrumentation"
        )
        self.assertTrue(
            h5file["entry"]["instruments"]["INSTRUMENT_1"].attrs["name"]
            == "Instrument name"
        )

        h5file.close()

    # Testing filters
    def test_acquire_s_filters(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_filters.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)

        enexar.acquire_s(
            filename,
            self.test_pv_name,
            ts=True,
            dbnd={"d": 1.5},
            arr={"i": 1},
            sync={"while": "blue"},
            dec=1,
        )

        enexar.stop(filename, self.test_pv_name)

    #  Non existing PV async
    def test_acquire_error(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_error.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        enexar.acquire(filename, "ca://PV", timeout=1)

        # Delay to make sure the acquisition started
        while bool(enexar.is_acquiring(filename, "ca://PV")) is False:
            time.sleep(0.1)

        self.assertTrue(enexar.is_acquiring(filename, "ca://PV"))
        # Wait for timeout
        time.sleep(5)
        self.assertFalse(enexar.is_acquiring(filename, "ca://PV"))

    #  Non existing PV sync
    def test_acquire_s_error(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_error.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        with self.assertRaises(Exception):
            enexar.acquire_s(filename, "pva://PV")

    #  Non name PV sync
    def test_acquire_s_no_pv_name(self):
        enexar = EnexarCli(self.prefix)
        filename = "test_acquire_s_no_pv_name.h5"
        path = os.path.join(self.root_path, filename)
        if os.path.exists(path):
            os.remove(path)
        with self.assertRaises(Exception):
            enexar.acquire_s(filename, "")

    ###########
    # Test LS #
    ###########

    def test_ls_current_dir(self):
        enexar = EnexarCli(self.prefix)
        ls_out = enexar.ls(".")
        self.assertTrue(isinstance(ls_out, list))

    def test_ls_nonexisting(self):
        enexar = EnexarCli(self.prefix)
        with self.assertRaises(Exception):
            enexar.ls("no_name")

    def test_ls_file(self):
        open(os.path.join(self.root_path, "test_file.txt"), "a").close()
        enexar = EnexarCli(self.prefix)
        self.assertEqual(enexar.ls("test_file.txt")[0], "test_file.txt")


if __name__ == "__main__":
    with open("junit.xml", "wb") as output:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output))
