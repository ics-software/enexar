#!/usr/bin/env python

from setuptools import setup

setup(
    name="enexar",
    version="2.5",
    description="ENeXAr Server",
    author="Juan F. Esteban Müller",
    author_email="JuanF.EstebanMuller@ess.eu",
    url="https://gitlab.esss.lu.se/ics-software/enexar",
    packages=["enexar"],
    python_requires=">3.8",
    install_requires=[
        "p4p",
        "numpy",
        "h5py",
        "pyepics",
        "ess_nexus",
        "datetime",
        "JPype1",
        "oxal",
        "psutil",
    ],
    entry_points={
        "console_scripts": ["enexar=enexar.enexar:main"],
    },
)
