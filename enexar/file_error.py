#!/usr/bin/env python
# -*- coding: utf-8 -*-


class FileError(Exception):
    """
    Generic error during file manipulations.
    """

    pass
