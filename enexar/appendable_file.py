#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py
from essnexus.essnexus import File


def check_file_extension(file_path: str) -> str:
    # Appending h5 extension if missing
    if not file_path.endswith(".h5") and not file_path.endswith(".hdf5"):
        file_path += ".h5"
    return file_path


class AppendableFile(File):
    """
    Creates a File object for an existing file.
    """

    def __init__(self, filename):
        """
        :param filename: name of HDF5 file
        :type filename: str, optional
        """
        self.filename = filename
        self.h5file = h5py.File(filename, "r+")
        self.nxentry = self.h5file.get("entry")
