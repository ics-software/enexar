#!/usr/bin/env python
# -*- coding: utf-8 -*-


from multiprocessing import get_context
from multiprocessing.queues import SimpleQueue
from threading import Event, Lock, Thread
from typing import List

from enexar.pv_handler_pool import PvHandlerPool


class PvHandlerProxy(object):
    """
    Class to send tasks from PvManager objects to a PvHandlerWorker from a PvHandlerPool, so that the worker can process the task.
    """

    def __init__(self, writer_queues: List[SimpleQueue]):
        # Create pool
        self.start_pool(writer_queues)

    def start_pool(self, writer_queues: List[SimpleQueue]):
        self.mp_ctxt = get_context("spawn")
        self.queue = self.mp_ctxt.SimpleQueue()
        PvHandlerProxyLoop.new_proxy_loop(self)

        self.pool = PvHandlerPool(writer_queues)

    def stop_pool(self):
        PvHandlerProxyLoop.stop_loop()

        if self.pool is not None:
            self.pool.stop()

        self.queue = None

    def acquire(
        self,
        pv: str,
        filters: str,
        path: str,
        n_acq: int,
        continuous: bool,
        timeout: float,
        writer_idx: int,
    ) -> bool:
        if self.queue is None:
            print(
                f"Received acquire command for {pv},{path}, but Pv Handler Pool not running!"
            )
            return False
        try:
            self.queue.put(
                (
                    "acquire",
                    pv,
                    path,
                    filters,
                    n_acq,
                    continuous,
                    timeout,
                    writer_idx,
                )
            )
            return True
        except Exception as e:
            print(e)
            return False

    def acquisition_processed(
        self, pv: str, path: str, acq_done_event_id: int, discarded: bool = False
    ):
        if self.queue is None:
            print(
                f"Received acquisition_processed command for {pv},{path}, but Pv Handler Pool not running!"
            )
            return
        self.queue.put(
            ("acquisition_processed", pv, path, acq_done_event_id, discarded)
        )

    def stop_acquisition(self, pv: str, path: str):
        if self.queue is None:
            print(
                f"Received stop_acquisition command for {pv},{path}, but Pv Handler Pool not running!"
            )
            return

        self.queue.put(("stop_acquisition", pv, path))


class PvHandlerProxyLoop(object):
    """
    This class runs a loop on a separate thread that sends commands to the PvHandlerWorkers
    """

    loop = None

    def __init__(self, proxy: PvHandlerProxy):
        self.proxy = proxy
        self.stop_event = Event()

        self.thread = Thread(target=self._loop)
        self.thread.start()

    @classmethod
    def new_proxy_loop(cls, proxy: PvHandlerProxy):
        if cls.loop is not None:
            print(
                "PvHandlerProxyLoop.new_proxy_loop() error: the loop is already created"
            )

        cls.loop = cls(proxy)

        return cls.loop

    def _loop(self):
        """
        A loop is required in the pool because it resides in the main process and a queue is used to communicate jobs to the workers.
        """

        self.lock = Lock()
        self.pv_handler_workers = dict()

        while not self.stop_event.is_set():
            try:
                if self.proxy.queue is None:
                    print(
                        "Error in PvHandlerProxyLoop._loop(): PvHandlerProxyLoop not running!"
                    )
                    return
                (command, pv, path, *args) = self.proxy.queue.get()
                if command is not None:
                    with self.lock:
                        if command == "acquire":
                            self._acquire(pv, path, *args)
                        elif command == "acquisition_processed":
                            self._acquisition_processed(pv, path, *args)
                        elif command == "stop_acquisition":
                            self._stop_acquisition(pv, path)
            except Exception:
                print(
                    "Exception in PvHandlerProxyLoop._loop()", command, pv, path, args
                )

    @classmethod
    def stop_loop(cls):
        if cls.loop is None:
            print(
                "Exception PvHandlerProxyLoop.stop_loop(): PvHandlerProxyLoop not running!"
            )
            return

        # Stop the thread listening to new commands for workers.
        cls.loop.stop_event.set()
        cls.loop.proxy.queue.put((None, None, None))

        if cls.loop.thread is None:
            print("Exception PvHandlerPool.stop_pool(): loop thread does not exists!")
            return

        cls.loop.thread.join()
        cls.loop.thread = None

        cls.loop = None

    def _acquire(self, pv, path, *args):
        worker = self.proxy.pool.get_worker()
        worker.acquire(pv, path, *args)
        self.pv_handler_workers[(pv, path)] = worker

    def _acquisition_processed(self, pv: str, path: str, *args):
        if (pv, path) in self.pv_handler_workers:
            worker = self.pv_handler_workers[(pv, path)]
            worker.acquisition_processed(pv, path, *args)
        else:
            print(
                "WARNING: received acquisition_processed command for PV {} and file {}, which is not being acquired".format(
                    pv, path
                )
            )

    def _stop_acquisition(self, pv: str, path: str):
        worker = self.pv_handler_workers.get((pv, path))
        if worker is not None:
            worker.stop(pv, path)
