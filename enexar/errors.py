#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from typing import Any, Optional

from p4p import Value
from p4p.nt import NTScalar


def _get_error(datatype: str, value: Optional[Any], message: str) -> Value:
    """
    Generates a NTScalar object of a given type with an active alarm, including an error message.

    :param datatype: data type, as defined by the p4p module https://mdavidsaver.github.io/p4p/values.html#p4p.Type .
    :type datatype: str
    :param value: value for the NTScalar.
    :type value: any
    :param message: content of the alarm message.
    :type message: str
    """
    errortype = NTScalar.buildType(datatype)
    error = Value(errortype, {"value": value})
    error.alarm.status = 1
    error.alarm.severity = 2
    error.alarm.message = message

    current_time_ns = time.time_ns()
    error.timeStamp.secondsPastEpoch = current_time_ns // 1000000000
    error.timeStamp.nanoseconds = current_time_ns % 1000000000
    return error
