#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from multiprocessing import SimpleQueue, cpu_count, get_context
from typing import List

from enexar.file_worker import FileManagerWorker
from enexar.pv_handler_proxy import PvHandlerProxy


class FileWorkerPool(object):
    """
    Pool of processes running a FileManagerWorker object which can take acquisitions.
    """

    def __init__(self, status_queue: SimpleQueue, task_done_queue: SimpleQueue):
        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        self.mp_ctxt = get_context("spawn")

        self.event = self.mp_ctxt.Event()
        self.status_queue = status_queue
        # Writer queue is where all PvHandler objects belonging to a FileManagerWorker send data to be saved to a file.
        # The queues are created before the PvHandlerPool is instantiated and are made available to the PvHandlerWorker objects through the PvHandlerProxy.
        self.writer_queues = []
        self.task_done_queue = task_done_queue

        self.n_proc = cpu_count()
        for _ in range(self.n_proc):
            self.writer_queues.append(self.mp_ctxt.SimpleQueue())

        self.pv_handler_proxy = PvHandlerProxy(self.writer_queues)
        self.processes: List[FileManagerWorker] = []

    def start(self):
        if self.processes == []:
            t0 = time.time()
            self.next_process = 0

            for i in range(self.n_proc):
                self.processes.append(
                    FileManagerWorker(
                        i,
                        self.event,
                        self.writer_queues[i],
                        self.pv_handler_proxy,
                        self.status_queue,
                        self.task_done_queue,
                    )
                )
            print(
                "Process pool with {0} {1} processes created in {2} seconds".format(
                    self.n_proc, FileManagerWorker.__name__, time.time() - t0
                )
            )

    def stop(self):
        # Stop the PvHandlerPool
        self.pv_handler_proxy.stop_pool()

        # Stop the workers
        self.event.set()

        for worker in self.processes:
            worker.interrup()

        self.processes = []

        self.pool = None

    def get_worker(self):
        worker = self.processes[self.next_process]
        self.next_process += 1
        self.next_process %= self.n_proc
        return worker
