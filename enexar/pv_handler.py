#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
from multiprocessing.queues import SimpleQueue
from multiprocessing.shared_memory import SharedMemory
from threading import Event, Lock, Semaphore

from epics import PV
from p4p.client.thread import Context

from enexar.task_completion_manager import TaskCompletionManager

CA_PREFIX = "ca://"
PVA_PREFIX = "pva://"


# Maximum number of events that a PvHandler object can queue to the writer.
max_events = 14


class PvHandler(object):
    """
    This class manages the connection to a PV and creates a monitor.
    """

    def __init__(
        self,
        pv: str,
        path: str,
        filters: str,
        n_acq: int,
        continuous: bool,
        timeout: float,
        writer_queue: SimpleQueue,
        pva_ctxt: Context,
    ):
        self.pv = pv
        self.pv_w_prefix = pv
        if pv.startswith(CA_PREFIX):
            self.pv = pv[len(CA_PREFIX) :]
        if pv.startswith(PVA_PREFIX):
            self.pv = pv[len(PVA_PREFIX) :]
        self.path = path
        self.filters = filters
        self.n_acq = int(float(n_acq))
        self.continuous = continuous
        self.timeout = float(timeout)
        self.semaphore = Semaphore(max_events)
        self.writer_queue = writer_queue
        self.ctxt = pva_ctxt

        self.acq_count = 0
        self.callback_count = 0

        self.lock = Lock()
        self.event = Event()
        self.stop_event = Event()

        self.timestamp: float = 0

        # Create a manager to keep track of received data.
        self._acqs_done_manager = TaskCompletionManager()

    @classmethod
    def get_handler(cls, *args):
        if args[0].startswith(CA_PREFIX):
            return CaPvHandler(*args)
        else:
            return PvaPvHandler(*args)

    def _start_monitor(self):
        raise NotImplementedError("Not Implemented")

    def _stop_monitor(self):
        raise NotImplementedError("Not Implemented")

    def acquire(self):
        """
        This method starts a monitor on a PV, with a timeout for each acquisition.

        At the end it triggers an acquisiton_done event for the PvManager.
        """
        try:
            self._start_monitor()

            no_timeout_flag = True

            # If timeout is 0, disable timeout.
            if self.timeout == 0:
                self.timeout = None

            # Wait for a timeout between each acquisition
            while no_timeout_flag and (self.continuous or self.acq_count < self.n_acq):
                no_timeout_flag = self.event.wait(self.timeout)

                if self.stop_event.is_set():
                    print(
                        "PvHandler.acquire received stop event for pv {} and path {}.".format(
                            self.pv, self.path
                        )
                    )
                    break

                self.event.clear()

            if not no_timeout_flag:
                print(
                    "PvHandler.acquire timed out for pv {} and path {}.".format(
                        self.pv, self.path
                    )
                )

            self._stop_monitor()
        except Exception as e:
            no_timeout_flag = False
            print(e)

        self.writer_queue.put(
            ("acquisition_done", self.path, self.pv_w_prefix, no_timeout_flag)
        )

    def acquisition_processed(self, acq_done_event_id: int, discarded: bool = False):
        if not discarded:
            with self.lock:
                self.acq_count += 1
        self._acqs_done_manager.done(acq_done_event_id)
        self.semaphore.release()

    def stop(self):
        self.stop_event.set()
        self.event.set()

    def callback(self, timestamp: float, value):
        acq_done_event_id = self._acqs_done_manager.new()
        self.event.set()
        # Acquire the semaphore to limit the number of events queued to the writer
        if self.semaphore.acquire(blocking=False):
            # Check that the received value is not the same as the previous one
            if timestamp == self.timestamp:
                self._acqs_done_manager.done(acq_done_event_id)
                self.semaphore.release()
                self.event.set()
                print(
                    "PvHandler received the same data twice for pv {}. Acquisition #{}".format(
                        self.pv_w_prefix, self.acq_count
                    )
                )
                return

            if not self.stop_event.is_set() and (
                self.continuous or self.acq_count < self.n_acq
            ):
                with self.lock:
                    self.timestamp = timestamp
                    try:
                        # Pickling the data and storing in shared memory
                        pickled_value = pickle.dumps(value)
                        size_value = len(pickled_value)
                        shm = SharedMemory(create=True, size=size_value)
                        shm.buf[:size_value] = pickled_value
                        shm.close()

                        self.writer_queue.put(
                            (
                                "save",
                                self.path,
                                self.pv_w_prefix,
                                timestamp,
                                shm,
                                size_value,
                                acq_done_event_id,
                            )
                        )
                    except Exception as e:
                        print(
                            "Exception in PvHandler._callback() during queue.put() method.\n",
                            e,
                        )

                # Wait for the data to be processed before exiting the Thread...
                self._acqs_done_manager.wait(acq_done_event_id)


class CaPvHandler(PvHandler):
    def _start_monitor(self):
        self.monitor = PV(
            self.pv + self.filters, callback=self._callback, auto_monitor=True
        )

    def _stop_monitor(self):
        self.monitor.clear_auto_monitor()
        self.monitor.clear_callbacks()
        # self.monitor.disconnect()

    def _callback(self, value, timestamp: float, **kw):
        self.callback(timestamp, value)


class PvaPvHandler(PvHandler):
    def _start_monitor(self):
        self.monitor = self.ctxt.monitor(self.pv + self.filters, self._callback)

    def _stop_monitor(self):
        self.monitor.close()

    def _callback(self, value):
        timestamp = (
            value.timeStamp.secondsPastEpoch + value.timeStamp.nanoseconds * 1e-9
        )
        ds_value = value.todict("value")
        self.callback(timestamp, ds_value)
