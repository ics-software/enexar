#!/usr/bin/env python
# -*- coding: utf-8 -*-

import inspect
from typing import Optional
from p4p.client.raw import RemoteError
from p4p.rpc import NTURIDispatcher
from p4p.rpc import rpc as _rpc
from p4p.wrapper import Value


def rpc(rtype=None, name: Optional[str] = None):
    """Decorator marks a method for export.

    Extended decorator to support declaring a different name for the rpc call than the method name.

    :param name: Specifies the name of the method for the rpc call.
    :param type: Specifies which :py:class:`Type` this method will return.

    >>> class Example(object):
        @rpc(name='ADD', rtype=NTScalar.buildType('d'))
        def add(self, lhs, rhs):
            return {'value':float(lhs)+flost(rhs)}
    """

    wrapper = _rpc(rtype)

    def named_wrapped(fn):
        fn = wrapper(fn)
        if name is not None:
            fn._method_name = name

        return fn

    return named_wrapped


class RpcDispatcherWithAttr(NTURIDispatcher):

    """
    Same as NTURIDispatcher, but methods can be named differently from the rpc call and it returns also the user and host generating the rpc call.
    """

    def __init__(self, queue, target=None, prefix=None, **kws):
        NTURIDispatcher.__init__(
            self, queue, prefix, target=target, **kws
        )  # we are our own Handler

        # Rebuild the methods dictionary
        M = self.methods = {}
        for name, mem in inspect.getmembers(target):
            if not hasattr(mem, "_reply_Type"):
                continue
            if hasattr(mem, "_method_name"):
                M[prefix + mem._method_name] = mem
            else:
                M[prefix + name] = mem

        self.channels = set(self.methods.keys())

    def _handle(self, op):
        try:
            request = op.value()
            name, args = self.getMethodNameArgs(request)
            fn = self.methods[name]
            rtype = fn._reply_Type

            # Adding username and host address to the list of arguments.
            args["user"] = op.account()
            args["host"] = op.peer()

            R = fn(**args)

            if not isinstance(R, Value):
                try:
                    R = Value(rtype, R)
                except Exception:
                    op.done(error="Error encoding reply")
                    return
            op.done(R)

        except RemoteError as e:
            print(e)
            op.done(error=str(e))

        except Exception:
            op.done(error="Error handling RPC")
