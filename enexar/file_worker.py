#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pickle
from multiprocessing import SimpleQueue, get_context
from multiprocessing.shared_memory import SharedMemory
from multiprocessing.synchronize import Event
from threading import Lock, Thread
from typing import Any, Dict, Tuple, Union

from enexar.file_error import FileError
from enexar.file_manager import FileManager
from enexar.pv_handler_proxy import PvHandlerProxy
from enexar.task_completion_manager import IdType


class FileManagerWorker(object):
    """
    A worker that runs an infinite loop listening to a queue and executes commands on separate Threads.

    Each command has 2 methods, one to be called by the main process and the other one (with a name that starts with underscore) to be executed by the worker process.
    """

    def __init__(
        self,
        idx: int,
        event: Event,
        writer_queue: SimpleQueue,
        pv_handler_proxy: PvHandlerProxy,
        status_queue: SimpleQueue,
        task_done_queue: SimpleQueue,
    ):
        self.idx = idx
        self.event = event
        # The writer loop thread will listen to this queue that gets data from PvHandler objects.
        self.writer_queue = writer_queue
        self.pv_handler_proxy = pv_handler_proxy
        self.status_queue = status_queue
        self.task_done_queue = task_done_queue

        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        mp_ctxt = get_context("spawn")
        self.process = mp_ctxt.Process(target=self._loop, args=(FileManager.timeout,))
        self.process.start()

    def _loop(self, timeout: float):
        # To keep track of the file managers that are currently handled by this worker.
        self.file_managers_lock = Lock()
        self.file_managers: Dict[str, FileManager] = dict()

        # Set timeout for the FileManagers
        FileManager.timeout = timeout

        try:
            while not self.event.is_set():
                try:
                    args = self.writer_queue.get()
                    if args is not None:
                        task_thread = Thread(target=self.task, args=args)
                        task_thread.start()
                except Exception as e:
                    print("FileManagerWorker.writer_queue error:", e)

        except (KeyboardInterrupt, SystemExit):
            print(
                "FileManagerWorker._loop finishes due to KeyboardInterrupt or SystemExit"
            )
            raise
        finally:
            # Stop writer loop thread and wait until it finishes.
            self.writer_queue.put(None)

    def interrup(self):
        """
        Stop the worker process.
        Running Threads will keep running. To stop them, use stop_all() instead.
        The event is set by the caller of this method, so that it is set only once for all workers.
        """
        self.writer_queue.put(None)
        self.process.join()
        print("Finishing FileManagerWorker process #{}".format(self.idx))

    def task(self, command: str, *args):
        try:
            if command == "check_timeout":
                self._check_timeout(*args)
            elif command == "stop_all":
                self._stop_all()
            elif command == "open":
                self._open(*args)
            elif command == "is_open":
                self._is_open(*args)
            elif command == "close":
                self._close(*args)
            elif command == "is_acquiring":
                self._is_acquiring(*args)
            elif command == "stop":
                self._stop(*args)
            elif command == "acquire":
                self._acquire(*args)
            elif command == "acquisition_done":
                self._acquisition_done(*args)
            elif command == "save":
                self._save(*args)
        except Exception as e:
            print(
                "FileWorker.task() exception for command {} with args".format(command),
                args,
                e,
            )

    def check_timeout(self, task_id: int):
        """
        Check if any file manager running in this process has timed out.
        Returns the list of files that have timed out.
        """
        self.writer_queue.put(("check_timeout", task_id))

    def _check_timeout(self, task_id: int):
        timed_out_files = []
        with self.file_managers_lock:
            for file_manager in list(self.file_managers.keys()):
                if self.file_managers[file_manager].has_timeout():
                    timed_out_files.append(self.file_managers[file_manager].path)
                    self.file_managers[file_manager].stop_all()
                    self.status_queue.put(
                        ("rm_file", self.file_managers[file_manager].path)
                    )
                    self.file_managers.pop(file_manager)

        self.task_done_queue.put(
            (
                task_id,
                timed_out_files,
            )
        )

    def stop_all(self):
        """
        Stop all acquisitions running in this worker process.
        This does not stop the worker process.
        """
        self.writer_queue.put(("stop_all",))

    def _stop_all(self):
        with self.file_managers_lock:
            for file_manager in self.file_managers.values():
                file_manager.stop_all()
            self.file_managers.clear()

    def open(self, root_path: str, file_path: str, task_id: int):
        self.writer_queue.put(("open", root_path, file_path, task_id))

    def _open(self, root_path: str, file_path: str, task_id: int):
        try:
            with self.file_managers_lock:
                file_manager = self.file_managers.get(file_path)
                if file_manager is None:
                    file_manager = FileManager(
                        root_path,
                        file_path,
                        self.status_queue,
                        self.pv_handler_proxy,
                        self.idx,
                    )
                    self.file_managers[file_path] = file_manager
        except Exception as e:
            message = "ERROR: " + str(e)
            result = (False, message)
            self.task_done_queue.put((task_id, result))
            print("FileManagerWorker._open ", message)
            return

        self.task_done_queue.put((task_id, True))

    def close(self, file_path: str, task_id: int):
        self.writer_queue.put(("close", file_path, task_id))

    def _close(self, file_path: str, task_id: int):
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                message = "ERROR: File not being acquired."
                result = (False, message)
                self.task_done_queue.put((task_id, result))
            else:
                file_manager.stop_all()
                self.file_managers.pop(file_path)
                self.task_done_queue.put((task_id, True))

    def is_open(self, file_path: str, task_id: int):
        self.writer_queue.put(("is_open", file_path, task_id))

    def _is_open(self, file_path: str, task_id: int):
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                result = False
            else:
                result = True

        self.task_done_queue.put((task_id, result))

    def is_acquiring(self, pv: str, file_path: str, task_id: int):
        self.writer_queue.put(("is_acquiring", pv, file_path, task_id))

    def _is_acquiring(self, pv: str, file_path: str, task_id: int):
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                result = False
            else:
                result = file_manager.is_acquiring(pv)

        self.task_done_queue.put((task_id, result))

    def stop(self, pv: str, file_path: str, task_id: int):
        self.writer_queue.put(("stop", pv, file_path, task_id))

    def _stop(self, pv: str, file_path: str, task_id: int):
        result: Union[bool, Tuple[bool, str]]
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                message = (
                    "ERROR in FileWorker._stop(): File {} not being acquired.".format(
                        file_path
                    )
                )
                result = (False, message)
            else:
                file_manager.stop(pv)
                result = True

        self.task_done_queue.put((task_id, result))

    def acquire(
        self,
        root_path: str,
        file_path: str,
        pv: str,
        use_groups: bool,
        timeout: float,
        n_acq: int,
        metadata: Dict[str, Any],
        filters: Dict[str, Any],
        continuous: bool,
        synchronous: bool,
        task_id: IdType,
    ):
        try:
            self.writer_queue.put(
                (
                    "acquire",
                    root_path,
                    file_path,
                    pv,
                    use_groups,
                    timeout,
                    n_acq,
                    metadata,
                    filters,
                    continuous,
                    synchronous,
                    task_id,
                )
            )
        except Exception as e:
            print("FileManagerWorker.acquire Exception:", e)
            raise e

    def _acquire(
        self,
        root_path: str,
        file_path: str,
        pv: str,
        use_groups: bool,
        timeout: float,
        n_acq: int,
        metadata: Dict[str, Any],
        filters: Dict[str, Any],
        continuous: bool,
        synchronous: bool,
        task_id: IdType,
    ):
        try:
            file_manager = self._get_file_manager(root_path, file_path)
            acq_result = file_manager.acquire(
                pv,
                timeout,
                n_acq,
                use_groups,
                metadata,
                filters,
                continuous,
                synchronous,
            )

            if acq_result:
                self.task_done_queue.put((task_id, acq_result))
            else:
                message = "Timeout while acquiring {}.".format(pv)
                self.task_done_queue.put((task_id, (acq_result, message)))
        except Exception as e:
            message = "ERROR: " + str(e)
            self.task_done_queue.put((task_id, (False, message)))
            print("FileManagerWorker._acquire Exception: ", message)

    def _acquisition_done(self, file_path: str, pv: str, no_timeout_flag: bool):
        file_manager = self.file_managers.get(file_path)
        if file_manager is None:
            message = "ERROR in FileWorker._acquisition_done(): File {} not being acquired.".format(
                file_path
            )
            print(message)
        else:
            file_manager.acquisition_done(pv, no_timeout_flag)

    def _save(
        self,
        file_path: str,
        pv: str,
        timestamp: float,
        shm: SharedMemory,
        size_value: int,
        acq_done_event_id: int,
    ):
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                message = (
                    "ERROR in FileWorker._save(): File {} not being acquired.".format(
                        file_path
                    )
                )
                print(message)
            else:
                value = pickle.loads(shm.buf[:size_value])
                shm.close()
                shm.unlink()
                file_manager.save(pv, timestamp, value, acq_done_event_id)

    def _get_file_manager(self, root_path: str, file_path: str) -> FileManager:
        # Getting FileManager for that file or create a new one.
        with self.file_managers_lock:
            file_manager = self.file_managers.get(file_path)
            if file_manager is None:
                if os.path.exists(os.path.join(root_path, file_path)):
                    message = "File already exists and is closed."
                    self.status_queue.put(("rm_file", file_path))
                    raise FileError(message)
                file_manager = FileManager(
                    root_path,
                    file_path,
                    self.status_queue,
                    self.pv_handler_proxy,
                    self.idx,
                )
                self.file_managers[file_path] = file_manager
            else:
                if not os.path.exists(os.path.join(root_path, file_path)):
                    message = "File {} was removed.".format(file_path)
                    self.status_queue.put(
                        ("rm_file", self.file_managers[file_path].path)
                    )
                    self.file_managers.pop(file_path)
                    raise FileError(message)

        return file_manager
