#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
ENeXAr - EPICS NeXuS Archiver

.. moduleauthor:: Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
"""

import argparse
import os
import sys

from .epics_server import EpicsServer
from .file_manager import FileManager


def main():
    # Parsing arguments.
    parser = argparse.ArgumentParser(
        description="ENeXAr is an EPICS RPC server that saves data from PVs to NeXuS files (HDF5). If run with no arguments, the default prefix is used and the data is stored in the current working directory."
    )
    parser.add_argument(
        "-r",
        "--root",
        default=os.path.curdir,
        help="path of the root folder where the data is stored. Default is the current working directory.",
    )
    parser.add_argument(
        "-p",
        "--prefix",
        default="ENEXAR:",
        help="Prefix for the PV names. Default is ENEXAR:",
    )
    parser.add_argument(
        "-t",
        "--timeout",
        default="0",
        help="Timeout in seconds after which the files are closed and don't accept more acquisitions. Default is 0, which is unlimited.",
    )

    # Try to parse the arguments.
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        sys.exit()

    root_path = args.root
    prefix = args.prefix

    FileManager.timeout = float(args.timeout)

    print("prefix = " + prefix)
    print("root_path = " + root_path)
    if not os.path.exists(root_path):
        print("Path to save Nexus files does not exists. Trying to create it...")
        try:
            os.makedirs(root_path)
        except Exception:
            print("Failed to create the directory to save the data.")
            raise

    # Instatiating and running the server.
    epics_server = EpicsServer(prefix, root_path)
    try:
        epics_server.start()
    except (KeyboardInterrupt, SystemExit):
        epics_server.stop()
        raise


if __name__ == "__main__":
    main()
