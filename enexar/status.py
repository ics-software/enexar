#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from multiprocessing import get_context
from threading import Lock, Thread
from typing import Optional

from p4p.nt import NTTable
from p4p.server.thread import SharedPV

from enexar.appendable_file import check_file_extension


class StatusException(Exception):
    """
    Exception used by the Status class to indicate an error in the received command.
    """

    pass


class Status(object):
    """
    This class keeps the status of all PVs being archived and updates the STATUS PV.
    """

    def __init__(self):
        status_table_type = NTTable.buildType(
            columns=[
                ("Path", "as"),
                ("PV", "as"),
                ("Mode", "as"),
                ("Acquired", "as"),
                ("User", "as"),
                ("Host", "as"),
                ("Time", "as"),
            ]
        )

        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        self.mp_ctxt = get_context("spawn")

        self.queue = self.mp_ctxt.SimpleQueue()

        self.updated = False
        self.lock = Lock()

        self.status_pv = SharedPV(initial=status_table_type({}))

        # This is a dictionary that contains pairs of an HDF5 file path and the PVs that are being saved on that file, together with a timestamp that monitors when was the last time the file was modified.
        self.status_dict = dict()
        self.status_to_pv()

        self.thread = Thread(target=self._loop)
        self.thread.start()

    def _loop(self):
        """
        This method waits to receive data from the queue to update the status PV from different subprocesses.
        """
        while True:
            try:
                (command, *args) = self.queue.get()
            except EOFError:
                break
            if command == "update":
                self._update_pv_acq(*args)
            elif command == "open":
                self._change_status_to_open(*args)
            elif command == "rm_file":
                self.rm_pv_from_status(*args)
            elif command == "stop":
                break
            else:
                raise StatusException("Received unknown command: ", command)

    def stop(self):
        try:
            self.queue.put(("stop",))
            self.thread.join()
        except Exception as e:
            print("Status.stop() Exception:", e)

        print("Finishing Status thread.")

    def status_to_pv(self):
        with self.lock:
            paths = []
            pvs = []
            modes = []
            n_acq = []
            users = []
            hosts = []
            times = []

            for path in self.status_dict.keys():
                if self.status_dict[path]["pvs"] == {}:
                    paths.append(path)
                    pvs.append("")
                    modes.append("OPEN")
                    n_acq.append("")
                    users.append("")
                    hosts.append("")
                    times.append(
                        time.strftime(
                            "%Y-%m-%d %H:%M:%S", self.status_dict[path]["timestamp"]
                        )
                    )
                else:
                    for pv in self.status_dict[path]["pvs"].keys():
                        paths.append(path)
                        pvs.append(pv)
                        modes.append(self.status_dict[path]["pvs"][pv]["mode"])
                        n_acq.append(self.status_dict[path]["pvs"][pv]["n_acq"])
                        users.append(self.status_dict[path]["pvs"][pv]["user"])
                        hosts.append(self.status_dict[path]["pvs"][pv]["host"])
                        times.append(
                            time.strftime(
                                "%Y-%m-%d %H:%M:%S",
                                self.status_dict[path]["pvs"][pv]["time"],
                            )
                        )

            # Update values in the PV
            current_status = self.status_pv.current()
            current_status.value.Path = paths
            current_status.value.PV = pvs
            current_status.value.Mode = modes
            current_status.value.Acquired = n_acq
            current_status.value.User = users
            current_status.value.Host = hosts
            current_status.value.Time = times

            current_time_ns = time.time_ns()
            current_status.timeStamp.secondsPastEpoch = current_time_ns // 1000000000
            current_status.timeStamp.nanoseconds = current_time_ns % 1000000000

            self.status_pv.post(current_status)

            self.updated = False

    def add_pv_to_status(self, path: str, pv: str, mode: str, user: str, host: str):
        path = check_file_extension(path)

        with self.lock:
            if self.status_dict.get(path) is None:
                self.status_dict[path] = {
                    "timestamp": time.localtime(),
                    "pvs": dict(),
                }
            self.status_dict[path]["pvs"][pv] = dict()

        self.update_pv_status(path, pv, mode, "0", user, host)

    def update_pv_status(
        self, path: str, pv: str, mode: str, n_acq: str, user: str, host: str
    ):
        path = check_file_extension(path)
        with self.lock:
            self.status_dict[path]["pvs"][pv]["mode"] = mode
            self.status_dict[path]["pvs"][pv]["n_acq"] = n_acq
            self.status_dict[path]["pvs"][pv]["user"] = user
            self.status_dict[path]["pvs"][pv]["host"] = host
            self.status_dict[path]["pvs"][pv]["time"] = time.localtime()

            self.updated = True

    def _update_pv_acq(self, path: str, pv: str, n_acq: str):
        with self.lock:
            if self.status_dict[path]["pvs"].get(pv) is not None:
                self.status_dict[path]["pvs"][pv]["n_acq"] = n_acq
                self.status_dict[path]["pvs"][pv]["time"] = time.localtime()
                self.updated = True

    def add_file(self, path: str):
        path = check_file_extension(path)

        with self.lock:
            self.status_dict[path] = {
                "timestamp": time.localtime(),
                "pvs": dict(),
            }

            self.updated = True

    def change_status_to_open(self, path: str, pv: str):
        path = check_file_extension(path)

        self.queue.put(("open", path, pv))

    def _change_status_to_open(self, path: str, pv: str):
        try:
            with self.lock:
                self.status_dict[path]["pvs"].pop(pv)
                self.status_dict[path]["timestamp"] = time.localtime()
                self.updated = True
        except Exception:
            pass

    def rm_pv_from_status(self, path: str, pv: Optional[str] = None):
        path = check_file_extension(path)

        try:
            with self.lock:
                self.status_dict.pop(path)
                self.updated = True
        except Exception:
            pass
