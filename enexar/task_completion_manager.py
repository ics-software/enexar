#!/usr/bin/env python
# -*- coding: utf-8 -*-

from threading import Event, Lock
from typing import Any, Dict, Optional, Tuple, Union

IdType = Union[int, Tuple[str, str]]


class TaskCompletionManager(object):
    """
    This class helps keeping track of when tasks are done.
    """

    def __init__(self):
        # List to store Event objects used to signal that a task was processed.
        self.events = dict()
        self.events_lock = Lock()
        # Dictionary to store results
        self.results: Dict[IdType, Optional[Any]] = dict()
        self.results_lock = Lock()

    def new(self, done_event_id: Optional[IdType] = None) -> IdType:
        """
        Create a new Event and return an id to it.
        """
        done_event = Event()
        if done_event_id is None:
            done_event_id = id(done_event)
        with self.events_lock:
            self.events[done_event_id] = done_event
        return done_event_id

    def done(self, done_event_id: IdType, result: Optional[Any] = None):
        """
        Mark a task with the given id as done and set the event.
        """
        with self.events_lock:
            done_event = self.events.get(done_event_id)
        with self.results_lock:
            self.results[done_event_id] = result
        if done_event is not None:
            done_event.set()

    def wait(self, done_event_id: IdType, no_pop: bool = False):
        """
        Wait for the task with the given id to be done. Finally pop the event.
        """
        with self.events_lock:
            done_event = self.events.get(done_event_id)
        if done_event is not None:
            done_event.wait()
            if no_pop is False:
                self.pop(done_event_id)
                return self.results.get(done_event_id)

        with self.results_lock:
            return self.results.pop(done_event_id, None)

    def pop(self, done_event_id: IdType) -> Optional[Any]:
        """
        Wait for the task with the given id to be done. Finally pop the event.
        """
        with self.events_lock:
            self.events.pop(done_event_id)
