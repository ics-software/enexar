#!/usr/bin/env python
# -*- coding: utf-8 -*-

from multiprocessing import get_context
from threading import Event, Lock, Thread
from typing import Any, Dict

from enexar.appendable_file import check_file_extension
from enexar.errors import _get_error
from enexar.file_worker_pool import FileWorkerPool
from enexar.status import Status
from enexar.task_completion_manager import TaskCompletionManager


class FileManagerProxy(object):
    """
    This class acts as a proxy to send commands from the main application process to the worker processes.
    """

    def __init__(self):
        # dict to keep track of which file is assigned to each worker
        self.workers = dict()
        self.workers_lock = Lock()

        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        self.mp_ctxt = get_context("spawn")

        # Create a manager to keep track of received data.
        self._task_done_manager = TaskCompletionManager()
        self.task_done_queue = self.mp_ctxt.SimpleQueue()

        self.task_manager_stop_event = Event()
        self.task_manager_thread = Thread(target=self._task_loop)
        self.task_manager_thread.start()

    def _task_loop(self):
        while not self.task_manager_stop_event.is_set():
            try:
                (task_id, *result) = self.task_done_queue.get()
                if task_id is not None:
                    self._task_done_manager.done(task_id, *result)
            except Exception:
                print("Exception in FileManagerProxy._task_loop()", task_id, *result)

    def stop_all(self):
        with self.workers_lock:
            for worker in self.workers.values():
                worker.stop_all()
            self.workers.clear()

        self.task_manager_stop_event.set()
        self.task_done_queue.put((None,))
        self.task_manager_thread.join()

    def check_timeout(self) -> bool:
        update = False
        with self.workers_lock:
            tasks = []
            for worker in self.workers.values():
                task_id = self._task_done_manager.new()
                worker.check_timeout(task_id)
                tasks.append(task_id)

            for task_id in tasks:
                time_out_files = self._task_done_manager.wait(task_id)
                if time_out_files is not None:
                    for f in time_out_files:
                        update = True
                        print("FileManagerProxy: file {} timed out".format(f))
                        self.workers.pop(f)

        return update

    def open(
        self, root_path: str, file_path: str, pool: FileWorkerPool, status: Status
    ):
        task_id = self._task_done_manager.new()

        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                worker = pool.get_worker()
                self.workers[file_path] = worker

            worker.open(root_path, file_path, task_id)

            result = self._task_done_manager.wait(task_id)

        if result is True:
            status.add_file(file_path)
        else:
            result = _get_error("?", *result)

        return result

    def close(self, file_path: str, status: Status):
        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                message = "ERROR: File not being acquired."
                return _get_error("?", False, message)

            task_id = self._task_done_manager.new()
            worker.close(file_path, task_id)
            self.workers.pop(file_path)
            result = self._task_done_manager.wait(task_id)

        if result is True:
            status.rm_pv_from_status(file_path)
        else:
            result = _get_error("?", *result)

        return result

    def is_open(self, file_path: str) -> bool:
        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                return False
            else:
                task_id = self._task_done_manager.new()
                worker.is_open(file_path, task_id)
                return self._task_done_manager.wait(task_id)

    def is_acquiring(self, pv: str, file_path: str) -> bool:
        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                return False
            else:
                task_id = self._task_done_manager.new()
                worker.is_acquiring(pv, file_path, task_id)
                return self._task_done_manager.wait(task_id)

    def stop(self, pv: str, file_path: str, status: Status):
        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                message = 'ERROR: PV "{}" not being acquired to file "{}".'.format(
                    pv, file_path
                )
                result = _get_error("?", False, message)
            else:
                task_id = self._task_done_manager.new()
                worker.stop(pv, file_path, task_id)
                result = self._task_done_manager.wait(task_id)

        if result is True:
            status.change_status_to_open(file_path, pv)
        else:
            result = _get_error("?", *result)

        return result

    def acquire(
        self,
        root_path: str,
        file_path: str,
        pv: str,
        use_groups: bool,
        timeout: float,
        n_acq: int,
        metadata: Dict[str, Any],
        filters: Dict[str, Any],
        continuous: bool,
        synchronous: bool,
        pool: FileWorkerPool,
    ):
        file_path = check_file_extension(file_path)

        with self.workers_lock:
            worker = self.workers.get(file_path)
            if worker is None:
                worker = pool.get_worker()
                self.workers[file_path] = worker

        task_id = self._task_done_manager.new()

        worker.acquire(
            root_path,
            file_path,
            pv,
            use_groups,
            timeout,
            n_acq,
            metadata,
            filters,
            continuous,
            synchronous,
            task_id,
        )

        result = self._task_done_manager.wait(task_id)

        if result is not None and result is not True:
            result = _get_error("?", *result)

        return result
