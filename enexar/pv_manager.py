#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
import logging
from multiprocessing.queues import SimpleQueue
from threading import Lock
from typing import Any, Dict, Optional

import h5py as hp

import numpy as np
from essnexus import File

from enexar.file_error import FileError
from enexar.pv_handler_proxy import PvHandlerProxy
from enexar.task_completion_manager import TaskCompletionManager

CA_PREFIX = "ca://"
PVA_PREFIX = "pva://"
compression_level = 4

logger = logging.getLogger(__name__)


class PvManager(object):
    """
    This class takes care of writing the data from the PV monitors to NeXus files.

    It communicates with a PvHandlerPool through a Queue, to create a PvHandler that will monitor the PV.
    """

    def __init__(
        self,
        pv: str,
        path: str,
        nexus_file: File,
        file_lock: Lock,
        n_acq: int,
        ds_name: Optional[str],
        use_groups: bool,
        metadata: Dict[str, Any],
        filters: Dict[str, Any],
        continuous: bool,
        status_queue: SimpleQueue,
        timeout: float,
        writer_idx: int,
        pv_handler_proxy: PvHandlerProxy,
    ):
        self.pv = pv

        if ds_name is None:
            self.ds_name = self.pv
            if pv.startswith(CA_PREFIX):
                self.ds_name = pv[len(CA_PREFIX) :]
            if pv.startswith(PVA_PREFIX):
                self.ds_name = pv[len(PVA_PREFIX) :]
        else:
            self.ds_name = ds_name

        self.metadata = metadata
        self.metadata["EPICS_protocol"] = "PVA"
        if pv.startswith(CA_PREFIX):
            self.metadata["EPICS_protocol"] = "CA"
        self.path = path
        self.nexus_file = nexus_file
        self.file_lock = file_lock
        self.n_acq = n_acq
        self.continuous = continuous
        self.status_queue = status_queue
        self.timeout = float(timeout)
        self.writer_idx = writer_idx
        self.pv_handler_proxy = pv_handler_proxy
        self.filters: str

        self.count = 0

        if self.nexus_file.h5file is None:
            raise FileError("HDF5 file could not be opened.")

        group = self.nexus_file.h5file.get("entry")
        if not isinstance(group, hp.Group):
            raise FileError(
                f"NeXus file has an 'entry' element that is not of 'Group' type. Type = {type(group)}."
            )

        if group is None:
            raise FileError("Existing NeXus file with wrong format.")
        nxentry: hp.Group = group

        data = nxentry.require_group("data")

        if use_groups:
            self.group = data.require_group(self.ds_name)
        else:
            self.group = data

        self.parse_filters(filters)

        # Create a manager to keep track of received data.
        self._acqs_done_manager = TaskCompletionManager()

    def parse_filters(self, filters: Dict[str, Any]):
        """
        Append a JSON string to the PV name to include EPICS filters.
        """
        self.filters = ""
        if len(filters) != 0:
            self.filters += r".{"
        for epics_filter in filters:
            if epics_filter == "ts":
                self.filters += r'"ts":{},'
                self.metadata["timestamp_filter"] = True
            elif epics_filter == "dbnd":
                self.filters += r'"dbnd":{'
                for param in filters[epics_filter]:
                    self.filters += (
                        '"' + param + '":' + str(filters[epics_filter][param]) + ","
                    )
                self.filters += r"},"
                self.metadata["deadband_filter"] = str(filters[epics_filter])
            elif epics_filter == "arr":
                self.filters += r'"arr":{'
                for param in filters[epics_filter]:
                    self.filters += (
                        '"' + param + '":' + str(filters[epics_filter][param]) + ","
                    )
                self.filters += r"},"
                self.metadata["array_filter"] = str(filters[epics_filter])
            elif epics_filter == "sync":
                cond = next(iter(filters[epics_filter]))
                state = filters[epics_filter][cond]
                self.filters += r'"sync":{"' + cond + '":"' + state + r'"},'
                self.metadata["synchronize_filter"] = str(filters[epics_filter])
            elif epics_filter == "dec" and filters["dec"] > 0:
                self.filters += r'"dec":{"n":' + str(filters[epics_filter]) + r"},"
                self.metadata["decimation_filter"] = filters[epics_filter]
        if len(filters) != 0:
            self.filters += r"}"

    def acquire(self) -> bool:
        """
        This method starts a monitor on a PV, with a timeout for each acquisition.
        """
        self._acqs_done_manager.new((self.pv, self.path))

        no_timeout_flag = self.pv_handler_proxy.acquire(
            self.pv,
            self.filters,
            self.path,
            self.n_acq,
            self.continuous,
            self.timeout,
            self.writer_idx,
        )

        if no_timeout_flag:
            no_timeout_flag = self._acqs_done_manager.wait(
                (self.pv, self.path), no_pop=True
            )

        return no_timeout_flag

    def acquisition_done(self, no_timeout_flag: bool):
        self._acqs_done_manager.done((self.pv, self.path), result=no_timeout_flag)

    def stop(self):
        self.pv_handler_proxy.stop_acquisition(self.pv, self.path)
        self._acqs_done_manager.wait((self.pv, self.path), no_pop=True)

    def save(self, timestamp: float, value, acq_done_event_id: int):
        """
        file_lock is acquired before calling this method.
        """
        if not self.continuous and self.count >= self.n_acq:
            # Inform PvHandler that the acquisition was discarded
            self.pv_handler_proxy.acquisition_processed(
                self.pv, self.path, acq_done_event_id, True
            )
            return

        # Adding index for multiple acquisitions
        ds_name = (
            self.ds_name
            + "_"
            + datetime.fromtimestamp(timestamp).strftime("%Y%m%d_%H%M%S_%f")
        )

        # Check whether the file already has a dataset with that name.
        # If it does, do not overwrite it. In some configurations, CA gateways return twice the same value.
        if self.group.get(ds_name) is not None:
            print("Dataset " + ds_name + " already exists")
            # Inform PvHandler that the acquisition was discarded
            self.pv_handler_proxy.acquisition_processed(
                self.pv, self.path, acq_done_event_id, True
            )
            return

        # Adding metadata to the dataset
        self.metadata["PV"] = self.pv
        self.metadata["time_stamp"] = timestamp

        try:
            self._parse_value(self.group, ds_name, value)

            self.count += 1

            # Inform PvHandler that the acquisition was saved
            self.pv_handler_proxy.acquisition_processed(
                self.pv, self.path, acq_done_event_id
            )

            self.status_queue.put(("update", self.path, self.pv, str(self.count)))
        except Exception as e:
            # Inform PvHandler that the acquisition was handled
            self.pv_handler_proxy.acquisition_processed(
                self.pv, self.path, acq_done_event_id, True
            )
            logger.error(
                "Couldn't save the dataset " + ds_name + " for PV " + self.pv + ".", e
            )

    def _parse_type(self, value):
        ds_value = value
        if type(ds_value) == np.ndarray:
            dataset_dtype = np.dtype(type(ds_value[0]))
        elif type(ds_value) == str:
            dataset_dtype = np.dtype(hp.string_dtype(encoding="utf-8"))
        else:
            dataset_dtype = np.dtype(type(ds_value))

        return dataset_dtype

    def _parse_value(self, parent, key, value):
        if isinstance(value, dict):
            # Enum or substructure
            if "index" in value.keys() and "choices" in value.keys():
                enum_type = hp.enum_dtype(
                    {f"{c}": i for i, c in enumerate(value["choices"])}, basetype="i"
                )
                parent.create_dataset(key, data=value["index"], dtype=enum_type)
            else:
                group = parent.create_group(name=key)
                for k, v in value.items():
                    self._parse_value(group, k, v)
        else:
            # Value
            if isinstance(value, np.ndarray):
                # Only compress arrays
                try:
                    parent.create_dataset(
                        key,
                        data=value,
                        compression="gzip",
                        compression_opts=compression_level,
                        dtype=self._parse_type(value),
                    )
                except Exception as e:
                    logger.error(
                        f"key={key}\nvalue={value}\ntype={self._parse_type(value)}"
                    )
                    raise e
            else:
                parent.create_dataset(
                    key,
                    data=value,
                    dtype=self._parse_type(value),
                )
