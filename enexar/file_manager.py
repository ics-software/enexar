#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
from multiprocessing import SimpleQueue
from threading import Lock, Thread
from typing import Any, Dict, Optional

from essnexus import essnexus

from enexar.file_error import FileError
from enexar.pv_handler_proxy import PvHandlerProxy
from enexar.pv_manager import PvManager

from .appendable_file import AppendableFile
from .errors import _get_error


class FileManager(object):
    """
    The FileManager class is used to keep track of the NeXus files.

    Each instance of FileManager is associated with a NeXus file, and it handles acquisitions to those files using PvHandler objects.
    """

    timeout: float = 0

    def __init__(
        self,
        root_path: str,
        path: str,
        status_queue: SimpleQueue,
        pv_handler_proxy: PvHandlerProxy,
        writer_idx: int,
    ):
        self.path = path
        self.hdfilename = os.path.join(root_path, path)
        self.status_queue = status_queue
        self.pv_handler_proxy = pv_handler_proxy
        self.writer_idx = writer_idx

        dirname = os.path.split(self.hdfilename)[0]
        if dirname != "" and not os.path.exists(dirname):
            try:
                os.makedirs(dirname)
            except Exception:
                raise FileError("Failed to create the directory to save the data.")

        # Lock for writing to file
        self.file_lock = Lock()

        self.timestamp = time.time()

        self.pv_managers: Dict[str, PvManager] = dict()

        # If the file does not exists yet, create it.
        if not os.path.exists(self.hdfilename):
            try:
                self.nexus_file = essnexus.File(self.hdfilename)
            except Exception:
                # If file cannot be opened, remove the file from the status table.
                self.status_queue.put(("rm_file", self.path))
                raise

            # Closing the file.
            self.nexus_file.close()

        self.nexus_file = None

    def save(self, pv: str, timestamp: float, value, acq_done_event_id: int):
        with self.file_lock:
            self.timestamp = timestamp
            pv_manager = self.pv_managers.get(pv)

        if pv_manager is None:
            print("ERROR: FileManager for path {} ".format(self.path))
            print("No PvManager for pv {0} . Was it closed?".format(pv))
        else:
            with self.file_lock:
                pv_manager.save(timestamp, value, acq_done_event_id)

    # Always acquire the lock before calling this method.
    def write_metadata(self, metadata: Dict[str, Any]):
        if self.nexus_file is None:
            raise FileError("NeXus file not open.")

        self.write_file_entry_metadata(metadata.get("file_entry"))

        self.write_file_user_metadata(metadata.get("file_user"))

        self.write_instruments_metadata(metadata.get("instruments"))

    def write_file_entry_metadata(self, file_entry: Optional[Dict[str, Any]]):
        if file_entry is None:
            return

        # Setting default values
        for key in [
            "run_cycle",
            "entry_identifier",
            "title",
            "description",
            "start_time",
        ]:
            if file_entry.get(key) is None:
                file_entry[key] = "None"

        self.nexus_file.create_file_entry(
            run_cycle=file_entry["run_cycle"],
            entry_identifier=file_entry["entry_identifier"],
            title=file_entry["title"],
            description=file_entry["description"],
            start_time=file_entry["start_time"],
        )

    def write_file_user_metadata(self, file_user: Optional[Dict[str, Any]]):
        if file_user is None:
            return

        # Setting default values
        for key in ["name", "role", "affiliation", "facility_user_id"]:
            if file_user.get(key) is None:
                file_user[key] = "None"

        self.nexus_file.create_file_user(
            name=file_user["name"],
            role=file_user["role"],
            affiliation=file_user["affiliation"],
            facility_user_id=file_user["facility_user_id"],
        )

    def write_instruments_metadata(self, instruments: Optional[Dict[str, Any]]):
        if instruments is None:
            return

        for instrument_name in instruments.keys():
            instrument = instruments[instrument_name]
            # Setting default values
            for key in [
                "nxtype",
                "name",
                "location",
                "description",
                "channel",
                "epics_channels",
            ]:
                if instrument.get(key) is None:
                    instrument[key] = "None"
            if instrument.get("value") is None:
                instrument["value"] = 0

            self.nexus_file.add_instrument(
                instrument_name,
                nxtype=instrument["nxtype"],
                name=instrument["name"],
                location=instrument["location"],
                description=instrument["description"],
                channel=instrument["channel"],
                epics_channels=instrument["epics_channels"],
                value=instrument["value"],
            )

            instrument_attributes = {
                att: instruments[instrument_name][att]
                for att in instruments[instrument_name]
                if att
                not in [
                    "nxtype",
                    "name",
                    "location",
                    "description",
                    "channel",
                    "epics_channels",
                    "value",
                ]
            }

            if instrument_attributes != {}:
                self.nexus_file.add_instrument_attributes(
                    instrument_name, instrument_attributes
                )

    def is_acquiring(self, pv: str) -> bool:
        return self.pv_managers.get(pv) is not None

    def acquire(
        self,
        pv: str,
        timeout: float,
        n_acq: int,
        use_groups: bool,
        metadata: Optional[Dict[str, Any]],
        filters: Dict[str, Any],
        continuous: bool,
        synchronous: bool,
    ) -> bool:
        with self.file_lock:
            if self.nexus_file is None:
                self.nexus_file = AppendableFile(self.hdfilename)

            # Initialise metadata
            ds_name: Optional[str] = None
            if metadata is not None:
                self.write_metadata(metadata)
                metadata = metadata.get("data")

            if metadata is not None and "dataset" in metadata.keys():
                ds_name = metadata.pop("dataset")

            if metadata is None:
                metadata = dict()

            pv_manager = PvManager(
                pv,
                self.path,
                self.nexus_file,
                self.file_lock,
                n_acq,
                ds_name,
                use_groups,
                metadata,
                filters,
                continuous,
                self.status_queue,
                timeout,
                self.writer_idx,
                self.pv_handler_proxy,
            )

            self.pv_managers[pv] = pv_manager

        if synchronous:
            return self._acquire(pv_manager)
        else:
            thread = Thread(target=self._acquire, args=(pv_manager,))
            thread.start()

        return True

    def _acquire(self, pv_manager: PvManager) -> bool:
        try:
            no_timeout_flag = pv_manager.acquire()
        except Exception as e:
            message = "ERROR: " + str(e)
            print(message)
            return _get_error("?", False, message)
        finally:
            with self.file_lock:
                self.pv_managers.pop(pv_manager.pv)
                self.close_nexus_if_needed()

            # Updating status PV after the file is closed.
            self.status_queue.put(("open", self.path, pv_manager.pv))

        return no_timeout_flag

    def acquisition_done(self, pv: str, no_timeout_flag: bool):
        pv_manager = self.pv_managers.get(pv)
        if pv_manager is not None:
            pv_manager.acquisition_done(no_timeout_flag)
        else:
            print(
                "ERROR: FileManager received acquisition_done event for a PV that is not being acquired."
            )

    # Stop the acquisition.
    def stop(self, pv: str):
        """
        Stop the acquisition. This will stop the PV monitor, but any acquisition that has been received and is in the writer queue will be saved to disk.
        """
        with self.file_lock:
            pv_manager = self.pv_managers.get(pv)
            if pv_manager is not None:
                pv_manager.stop()

    def stop_all(self):
        """
        Closing file and stopping all connections. After this method is called, the FileManager object cannot be used anymore.
        """
        with self.file_lock:
            for pv_manager in self.pv_managers.values():
                # Stop will trigger clearing of self.pv_managers dict
                pv_manager.stop()

    def close_nexus_if_needed(self):
        if self.pv_managers == dict():
            self.timestamp = time.time()
            if self.nexus_file is not None:
                try:
                    # Close may fail if the file was corrupted before opening it.
                    self.nexus_file.close()
                except Exception:
                    pass
                self.nexus_file = None

    # Return True if the file has not been used after a timeout.
    def has_timeout(self) -> bool:
        with self.file_lock:
            if (
                FileManager.timeout != 0
                and time.time() > self.timestamp + FileManager.timeout
            ):
                return True
            else:
                return False
