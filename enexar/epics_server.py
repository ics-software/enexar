#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
from typing import Any, Dict, Optional

import epicscorelibs.path.pyepics  # noqa: F401
import psutil
from p4p import Value
from p4p.nt import NTScalar
from p4p.server import Server, StaticProvider
from p4p.server.thread import SharedPV
from p4p.util import ThreadedWorkQueue

from enexar.file_worker_pool import FileWorkerPool

from .errors import _get_error
from .file_manager_proxy import FileManagerProxy
from .rpc_dispatcher_with_attr import RpcDispatcherWithAttr, rpc
from .status import Status

# Number of workers in the RPC server and maximum number of requests in the queue. These numbers need to be checked.
n_workers: int = 20
maxsize: int = 20

# Default timeout is 4 seconds, because by default the pvcall command will timeout in 5 seconds
# and we want to be able to return a timeout error.
DEFAULT_TIMEOUT = 4

# NOTE: the parameters of all the methods with @rpc decorators are always str or a dict containing str, because that is what the pvcall command sends.


class EpicsServer(object):
    def __init__(self, prefix: str, root_path: str):
        self.prefix = prefix
        self.root_path = root_path

    def start(self):
        self.stop_flag = False

        # RCP provider
        queue = ThreadedWorkQueue(maxsize=maxsize, workers=n_workers)
        rpc_provider = RpcDispatcherWithAttr(
            queue, target=self, prefix=self.prefix, name="ENeXAr"
        )

        # Status PV provider
        self.status = Status()
        status_pv_provider = StaticProvider("Status")
        status_pv_provider.add(self.prefix + "STATUS", self.status.status_pv)

        # Monitoring PVs
        self.cpu_usage_pv = SharedPV(nt=NTScalar("d"), initial=0.0)
        self.mem_usage_pv = SharedPV(nt=NTScalar("d"), initial=0.0)
        status_pv_provider.add(self.prefix + "CPU", self.cpu_usage_pv)
        status_pv_provider.add(self.prefix + "MEM", self.mem_usage_pv)

        # Instantiating the FileManagerProxy
        self.file_manager_proxy = FileManagerProxy()

        # Starting a Process Pool
        self.pool = FileWorkerPool(
            self.status.queue, self.file_manager_proxy.task_done_queue
        )
        self.pool.start()

        # Start the server
        static_server = Server(providers=[status_pv_provider])
        rpc_server = Server(providers=[rpc_provider])
        with static_server, rpc_server, queue:
            while not self.stop_flag:
                # Update Acquired column for status PV every 200 millisecond.
                for _ in range(50):
                    if self.stop_flag:
                        break
                    if self.status.updated:
                        self.status.status_to_pv()

                    self.cpu_usage_pv.post(psutil.cpu_percent())
                    self.mem_usage_pv.post(psutil.virtual_memory().percent)
                    time.sleep(0.2)

                # Check for timed out FileManagers and remove them from the status every 10 seconds.
                if self.file_manager_proxy.check_timeout():
                    self.status.status_to_pv()

    def stop(self):
        # Stop all acquisitions
        self.file_manager_proxy.stop_all()

        # Stop the worker processes
        self.pool.stop()

        # Stop the Thread updating the server PVs
        self.status.stop()

        # Stop updating the main loop updating the server status
        self.stop_flag = True

    @rpc(name="OPEN", rtype=NTScalar("?"))
    def open(self, path: str, user: str, host: str) -> Value:
        """
        Create a new FileManager for a file that doesn't exists. If the FileManager already exists, it does nothing.
        Everything is done by a Worker process on a different process.

        :param path: relative path of the file, including filename (extension optional).
        :type path: str
        :return: return True if command was successful, otherwise False with an alarm.
        :rtype: bool
        """
        return self.file_manager_proxy.open(
            self.root_path, path, self.pool, self.status
        )

    @rpc(name="IS_OPEN", rtype=NTScalar("?"))
    def is_open(self, path: str, user: str, host: str) -> Value:
        """
        Check if a file is already open.

        :param path: relative path of the file, including filename.
        :type path: str
        :return: return True if file is open, otherwise False.
        :rtype: bool
        """
        return self.file_manager_proxy.is_open(path)

    @rpc(name="CLOSE", rtype=NTScalar("?"))
    def close(self, path: str, user: str, host: str) -> Value:
        """
        Close a file. If there was any acquisition taking place, it is stopped.

        :param path: relative path of the file, including filename.
        :type path: str
        :return: return True if command was successful, otherwise False with an alarm.
        :rtype: bool
        """
        return self.file_manager_proxy.close(path, self.status)

    @rpc(name="ACQUIRE", rtype=NTScalar("?"))
    def acquire(
        self,
        path: str,
        pv: str,
        user: str,
        host: str,
        n_acq: str = "1",
        continuous: str = "False",
        use_groups: str = "True",
        timeout: Optional[str] = None,
        metadata: Optional[Value] = None,
        ts: Value = "False",
        dbnd: Optional[Value] = None,
        arr: Optional[Value] = None,
        sync: Optional[Value] = None,
        dec: Optional[Value] = None,
    ) -> Value:
        """
        Acquires a PV and save it into the file specified by path.
        By default, it takes only 1 acquisition, but can be increased by passing the n_acq parameter.
        It can also be used to run a continuous acquisition by setting the continuous=True that will ve valid until a STOP (or CLOSE) command arrives.
        This command is asyncronous, the acquisition is done in a separate thread and the rpc call will return inmediately if no problems are found.

        For the parameters and return value, check documentation of :py:meth:`._acquire`
        """
        return self._acquire(
            path,
            pv,
            n_acq,
            continuous,
            use_groups,
            timeout,
            metadata,
            ts,
            dbnd,
            arr,
            sync,
            dec,
            user,
            host,
        )

    @rpc(name="ACQUIRE_S", rtype=NTScalar("?"))
    def acquire_s(
        self,
        path: str,
        pv: str,
        n_acq: str = "1",
        continuous: str = "False",
        use_groups: str = "True",
        timeout: Optional[str] = None,
        metadata: Optional[Value] = None,
        ts: Value = "False",
        dbnd: Optional[Value] = None,
        arr: Optional[Value] = None,
        sync: Optional[Value] = None,
        dec: Optional[Value] = None,
        user: Optional[str] = None,
        host: Optional[str] = None,
    ) -> Value:
        """
        Acquires a PV and save it into the file specified by path.
        By default, it takes only 1 acquisition, but can be increased by passing the n_acq parameter.
        It can also be used to run a continuous acquisition by setting the continuous=True that will ve valid until a STOP (or CLOSE) command arrives.
        This command is syncronous, the rpc won't return until the acquisition is done. If the acquisiton takes too long, the rpc call may time out unless the timeout is increased to cover for the full acquisition time. This makes continuous synchronous acquisition in general not useful, but it is supported.

        For the parameters and return value, check documentation of :py:meth:`._acquire`
        """
        return self._acquire(
            path,
            pv,
            n_acq,
            continuous,
            use_groups,
            timeout,
            metadata,
            ts,
            dbnd,
            arr,
            sync,
            dec,
            user,
            host,
            synchronous=True,
        )

    def _acquire(
        self,
        path: str,
        pv: str,
        n_acq: str,
        continuous: str,
        use_groups: str,
        timeout: Optional[str],
        metadata: Optional[Value],
        ts: Value,
        dbnd: Optional[Value],
        arr: Optional[Value],
        sync: Optional[Value],
        dec: Optional[Value],
        user: Optional[str],
        host: Optional[str],
        synchronous: bool = False,
    ) -> Value:
        """
        :param path: relative path of the file, including filename.
        :param pv: PV signal to be acquired.
        :param n_acq: number of acquisitons, if continuous==False. Defaults to '1'.
        :param continuous: flag to enable continuous measurement. Defaults to False.
        :param timeout: timeout in seconds for each acquisition. Set to 0 to disable. Defaults to 4 seconds.
        :param metadata: metadata for the acquisition, in JSON format. See README.md for more information.
        :param ts: EPICS timestamp filter. If set to `True`, it will enable the timestamp filter that sets the timestamp to the time when the request was done, instead of when the record was processed.
        :param dbnd: EPICS deadband filter. To receive monitor updates only when the PV changes by an amount larger than the deadband. It is defined as a JSON string with the same format as in the EPICS documentation.
        :param arr: EPICS array filter. To get a subset of an array, defined by the starting index `s`, the end index `e`, and the increment `i`. It is defined as a JSON string with the same format as in the EPICS documentation. The shorthand notation is not accepted.
        :param sync: EPICS synchronize filter. To synchronize the monitor updates with respect to an internal IOC state. It is defined as a JSON string with the same format as in the EPICS documentation.
        :param dec: EPICS decimate filter. The monitor will update only after the record is processed `dec` times. It takes an integer number.
        :param synchronous: whether the acquisition is done synchronously (True) or asyncrhonously.

        :type path: str
        :type pv: str
        :type n_acq: str, optional
        :type continuous: str, optional
        :type timeout: str, optional
        :type metadata: dict, optional
        :type ts: bool, optional
        :type dbnd: dict, optional
        :type arr: dict, optional
        :type sync: dict, optional
        :type dec: str, optional
        :type synchronous: bool, optional

        :return: return True if the command is succesful, otherwise False with an alarm.
        :rtype: bool
        """
        if pv == "":
            message = "ERROR: no PV name specified."
            return _get_error("?", False, message)

        if self.file_manager_proxy.is_acquiring(pv, path):
            message = "PV already being acquired in the same file."
            return _get_error("?", False, message)

        # Processing filters
        try:
            filters: Dict[str, Any] = dict()
            if ts is not False and (
                ts.lower() == "true" or ts.lower() == "yes" or ts == "1"
            ):
                filters["ts"] = True
            if dbnd is not None:
                filters["dbnd"] = dbnd.todict()
            if arr is not None:
                filters["arr"] = arr.todict()
            if sync is not None:
                filters["sync"] = sync.todict()
            if dec is not None:
                filters["dec"] = int(dec)
        except Exception:
            message = "Filters could not be parsed. Please check."
            return _get_error("?", False, message)

        continuous_flag = continuous.lower() == "true" or continuous.lower() == "yes"
        if continuous:
            self.status.add_pv_to_status(path, pv, "CONT", user, host)
        else:
            self.status.add_pv_to_status(path, pv, "N_ACQ=" + n_acq, user, host)

        if timeout is None:
            # Timeout for continuous acquisition disabled by default
            if continuous_flag:
                timeout_f = 0.0
            else:
                timeout_f = DEFAULT_TIMEOUT
        else:
            timeout_f = float(timeout)

        use_groups_flag = use_groups.lower() == "true" or use_groups.lower() == "yes"

        if metadata is not None:
            metadata = metadata.todict()

        result = self.file_manager_proxy.acquire(
            self.root_path,
            path,
            pv,
            use_groups_flag,
            timeout_f,
            int(float(n_acq)),
            metadata,
            filters,
            continuous_flag,
            synchronous,
            self.pool,
        )

        return result

    @rpc(name="IS_ACQUIRING", rtype=NTScalar("?"))
    def is_acquiring(
        self, pv: str, path: str, user: Optional[str] = None, host: Optional[str] = None
    ) -> Value:
        """
        Returns True if the PV is being acuired to a given path.

        :param path: relative path of the file, including filename.
        :param pv: PV signal.
        :type path: str
        :type pv: str
        :return: return True if the PV is being acquired, otherwise False.
        :rtype: bool
        """
        return self.file_manager_proxy.is_acquiring(pv, path)

    @rpc(name="STOP", rtype=NTScalar("?"))
    def stop_acquisition(
        self, pv: str, path: str, user: Optional[str] = None, host: Optional[str] = None
    ) -> Value:
        """
        Stop logging a PV on a given path. The file will accept more measurements.

        :param path: relative path of the file, including filename.
        :param pv: PV signal.
        :type path: str
        :type pv: str
        :return: return True if the command is succesful, otherwise False with an alarm.
        :rtype: bool
        """
        return self.file_manager_proxy.stop(pv, path, self.status)

    @rpc(name="LS", rtype=NTScalar("as"))
    def list_directory(
        self, path: str = ",.", user: Optional[str] = None, host: Optional[str] = None
    ) -> Value:
        """
        List the directory passed as an argument, or the path if it is a file.

        :param path: relative path of the file, including filename.
        :type path: str
        :return: return the list of files in the directory passed as an argument, or the path if it is a file. If the path does not exist, it returns an Error message.
        :rtype: list
        """
        full_path = os.path.join(self.root_path, path)

        if not os.path.exists(full_path):
            message = "The given path does not exist."
            return _get_error("s", "Error", message)

        if os.path.isfile(full_path):
            return [path]

        try:
            return [elem for elem in os.listdir(full_path) if not elem.startswith(".")]
        except Exception as e:
            print(e)
