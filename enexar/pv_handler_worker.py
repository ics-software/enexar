#!/usr/bin/env python
# -*- coding: utf-8 -*-

from multiprocessing import SimpleQueue, get_context
from multiprocessing.synchronize import Event
from threading import Lock, Thread
from typing import Any, Dict, List

from p4p.client.thread import Context

from enexar.pv_handler import PvHandler


class PvHandlerWorker(object):
    """
    A worker that runs an infinite loop listening to a queue and executes tasks on separate Threads.

    Each tasks has 2 methods, one to be called by the PvHandlerPool (running in the main process) and the other one (with a name that starts with underscore) to be executed by the worker process.
    """

    def __init__(self, stop_event: Event, writer_queues: List[SimpleQueue]):
        self.stop_event = stop_event

        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        mp_ctxt = get_context("spawn")

        # Worker queue is a dedicated queue to pass tasks to the worker process. Only used by this class.
        self._worker_queue = mp_ctxt.SimpleQueue()
        self.writer_queues = writer_queues

        self.process = mp_ctxt.Process(target=self._loop)
        self.process.start()

    def _loop(self):
        self.pv_handlers = dict()
        self.pv_handlers_lock = Lock()

        # Get pva client context here, because it must be created in the same process where it will be used.
        self.ctxt = Context("pva", nt=False)

        try:
            while not self.stop_event.is_set():
                try:
                    args = self._worker_queue.get()
                    if args is not None:
                        task_thread = Thread(target=self._task, args=args)
                        task_thread.start()
                except Exception as e:
                    print("PvHandlerWorker._worker_queue error", self._worker_queue, e)

            print("PvHandlerWorker._worker_queue finished")
        except (KeyboardInterrupt, SystemExit):
            print(
                "PvHandlerWorker._loop finishes due to KeyboardInterrupt or SystemExit"
            )
            raise
        finally:
            self.ctxt.close()

    def _task(self, command: str, *args):
        try:
            if command == "acquire":
                self._acquire(*args)
            elif command == "acquisition_processed":
                self._acquisition_processed(*args)
            elif command == "stop":
                self._stop(*args)
        except Exception as e:
            print("Exception in PvHandlerWorker._task({},{}):".format(command, args), e)

    def interrup(self):
        """
        Stop the worker process.
        """
        self._worker_queue.put(None)
        self.process.join()

        print("Finishing PvHandlerWorker process.")

    def acquire(
        self,
        pv: str,
        path: str,
        filters: Dict[str, Any],
        n_acq: int,
        continuous: bool,
        timeout: float,
        writer_idx: int,
    ):
        self._worker_queue.put(
            (
                "acquire",
                pv,
                path,
                filters,
                n_acq,
                continuous,
                timeout,
                writer_idx,
            )
        )

    def _acquire(
        self,
        pv: str,
        path: str,
        filters: Dict[str, Any],
        n_acq: int,
        continuous: bool,
        timeout: float,
        writer_idx: int,
    ):
        pv_handler = PvHandler.get_handler(
            pv,
            path,
            filters,
            n_acq,
            continuous,
            timeout,
            self.writer_queues[writer_idx],
            self.ctxt,
        )
        with self.pv_handlers_lock:
            self.pv_handlers[(pv, path)] = pv_handler
        pv_handler.acquire()
        with self.pv_handlers_lock:
            self.pv_handlers.pop((pv, path))

    def acquisition_processed(self, pv: str, path: str, *args):
        self._worker_queue.put(("acquisition_processed", pv, path, *args))

    def _acquisition_processed(self, pv: str, path: str, *args):
        with self.pv_handlers_lock:
            pv_handler = self.pv_handlers.get((pv, path))
        if pv_handler is not None:
            pv_handler.acquisition_processed(*args)

    def stop(self, pv: str, path: str):
        self._worker_queue.put(("stop", pv, path))

    def _stop(self, pv: str, path: str):
        with self.pv_handlers_lock:
            pv_handler = self.pv_handlers.get((pv, path))
        if pv_handler is not None:
            pv_handler.stop()
