#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
from multiprocessing import cpu_count, get_context
from multiprocessing.queues import SimpleQueue
from typing import List

from enexar.pv_handler_worker import PvHandlerWorker


class PvHandlerPool(object):
    """
    Pool of processes running a PvHandlerWorker object which can take acquisitions.
    """

    processes: List[PvHandlerWorker] = []

    n_proc = cpu_count()
    next_process = 0

    def __init__(self, writer_queues: List[SimpleQueue]):
        t0 = time.time()

        # Using a 'spawn' multiprocessing context to avoid issues with EPICS contexts.
        self.mp_ctxt = get_context("spawn")

        self.workers_stop_event = self.mp_ctxt.Event()
        self.writer_queues = writer_queues

        for _ in range(self.n_proc):
            self.processes.append(
                PvHandlerWorker(self.workers_stop_event, writer_queues)
            )
        print(
            "Process pool with {0} {1} processes created in {2} seconds".format(
                self.n_proc, PvHandlerWorker.__name__, time.time() - t0
            )
        )

    def stop(self):
        # Stop worker processes
        self.workers_stop_event.set()
        for worker in self.processes:
            worker.interrup()

        self.processes = []

    def get_worker(self) -> PvHandlerWorker:
        worker = self.processes[self.next_process]
        self.next_process += 1
        self.next_process %= self.n_proc

        return worker
