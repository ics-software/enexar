.. ENeXAr documentation master file, created by
   sphinx-quickstart on Mon Dec 3 13:38:34 2020.

Welcome to ENeXAr's documentation!
======================================

Here you can find the commands that the RPC server can receive:

.. autoclass:: enexar.epics_server.EpicsServer
   :members:
   :private-members:
